#include "StaticLight.h"

// constructor taking in values for the light struct and assigning them using the various update functions
StaticLight::StaticLight(float ambR, float ambG, float ambB, float ambA, float diffR, float diffB, float diffG, float diffA, float specR, float specG, float specB, float specA, float posX, float posY, float posZ, float scale)
{
	updateAmbience(ambR, ambG, ambB, ambA);
	updateDiffuse(diffR, diffG, diffB, diffA);
	updateSpecular(specR, specG, specB, specA);
	updateLightStructPositionAndScalar(posX, posY, posZ, scale);
	updateLightVectorPositionAndScalar(posX, posY, posZ, scale);
}

// function that updates the StaticLight's light structures ambience values
void StaticLight::updateAmbience(float r, float g, float b, float a)
{
	light.ambient[0] = r;
	light.ambient[1] = g;
	light.ambient[2] = b;
	light.ambient[3] = a;
}

// function that updates the StaticLight's light structures diffuse values
void StaticLight::updateDiffuse(float r, float g, float b, float a)
{
	light.diffuse[0] = r;
	light.diffuse[1] = g;
	light.diffuse[2] = b;
	light.diffuse[3] = a;
}

// function that updates the StaticLight's light structures specular values
void StaticLight::updateSpecular(float r, float g, float b, float a)
{
	light.specular[0] = r;
	light.specular[1] = g;
	light.specular[2] = b;
	light.specular[3] = a;
}

// function that updates the StaticLights light structures position and scalar values
void StaticLight::updateLightStructPositionAndScalar(float posX, float posY, float posZ, float scale)
{
	light.position[0] = posX;
	light.position[1] = posY;
	light.position[2] = posZ;
	light.position[3] = scale;
}

// function that updates the StaticLights light vector position and scalar values
void StaticLight::updateLightVectorPositionAndScalar(float posX, float posY, float posZ, float scale)
{
	lightPositionAndScalar.x = posX;
	lightPositionAndScalar.y = posY;
	lightPositionAndScalar.z = posZ;
	lightPositionAndScalar.w = scale;
}

// function that returns the StaticLight Light structures ambience as a vec4
vec4 StaticLight::getLightStructAmbience()
{
	return vec4(light.ambient[0], light.ambient[1], light.ambient[2], light.ambient[3]);
}

// function that returns the StaticLight Light structures diffuse as a vec4
vec4 StaticLight::getLightStructDiffuse()
{
	return vec4(light.diffuse[0], light.diffuse[1], light.diffuse[2], light.diffuse[3]);
}

// function that returns the StaticLight Light structures specular as a vec4
vec4 StaticLight::getLightStructSpecular()
{
	return vec4(light.specular[0], light.specular[1], light.specular[2], light.specular[3]);
}

// function that returns the StaticLight light structures position as a vec4
vec4 StaticLight::getLightStructPositionAndScalar()
{
	return vec4(light.position[0], light.position[1], light.position[2], light.position[3]);
}

