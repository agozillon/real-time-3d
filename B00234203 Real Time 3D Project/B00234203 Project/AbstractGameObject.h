#ifndef ABSTRACTGAMEOBJECT_H
#define ABSTRACTGAMEOBJECT_H
#include "rt3d.h"               // rt3d so that classes that use this get access rt3d functions
#include "BoundingBox.h"		// including boundingbox header so we can have a BoundingBox object
#include <glm/gtc/type_ptr.hpp> // glm/gtc/type_ptr so the class can have access to vec3s
using namespace glm;			// using glm name space so I don't have to include variablename

// Abstract game object class that all game objects inherit from it specificys that they require functions to access positions/rotations/scalars and a bounding box pointer/object
class AbstractGameObject
{

public:
	virtual ~AbstractGameObject(){}														  // inline virtual destructor
	virtual vec3 getPosition() = 0;											     		  // virtual function that should be implemented to return the GameObjects position as a vec3
	virtual void updatePosition(vec3 pos) = 0;											  // virtual function that should be implemented to take in a vec3 and update the GameObjects position
	virtual vec3 getRotation() = 0;														  // virtual function that should be implemented to return the GameObjects rotation as a vec3
	virtual void updateRotation(vec3 rot) = 0;											  // virtual function that should be implemented to take in a vec3 and update the GameObjects rotation
	virtual vec3 getScalar() = 0;													      // virtual function that should be implemented to return the GameObjects scalar as a vec3
	virtual void updateScalar(vec3 scale) = 0;		                                      // virtual function that should be implemented to take in a vec3 and update the GameObjects scalar
	virtual void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix) = 0; // virtual function that should be implemented to take a shader/material/matrix and use them to draw the GameObject
	
	BoundingBox * collisionBox;					                                          // pointer to a bounding box class as I wish for derived classes to have at least one bounding box

};

#endif	

