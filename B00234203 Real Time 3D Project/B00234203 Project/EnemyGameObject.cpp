#include "EnemyGameObject.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms
#include <cmath>						// including cmath for the tan2 function	

// constructor that takes in position/rotation/scalar and meshID/IndexCount/textID and collisionBox and agroBox positions and dimensions then instantiates/assigns them
EnemyGameObject::EnemyGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions, vec3 agroBoxPos, vec3 agroBoxDimensions)
{
	// setting up the position/scalar/rotation
	position = pos;
	scalar = scale;
	rotation = rot;

	// constructing and setting up the collisionBox position and dimensions by passed in variables
	collisionBox = new BoundingBox(collisionBoxPos, collisionBoxDimensions);

	// constructing and setting up the agroBox position and dimensions by passed in variables
	agroBox = new BoundingBox(agroBoxPos, agroBoxDimensions);
	
	// constructing the ObjModelHolder by passing in the meshID/indexCount/textureID
	model = new ObjModelHolder(meshID, indexCount, textureID);
}

// destructor deleting all objects instantiated within this object
EnemyGameObject::~EnemyGameObject()
{
	delete model;
	delete collisionBox;
	delete agroBox;
}

// taking in a shader/material/matrix and drawing the EnemyGameObject to screen using the specified position/rotation/scalar
void EnemyGameObject::draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix)
{	
	mvMatrix = translate(mvMatrix, vec3(position.x, position.y, position.z));
	mvMatrix = rotate(mvMatrix, rotation.x, vec3(1.0f, 0.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.y, vec3(0.0f, 1.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.z, vec3(0.0f, 0.0f, 1.0f));
	mvMatrix = scale(mvMatrix, vec3(scalar.x, scalar.y, scalar.z));
	model->draw(shaderID, material,  mvMatrix);
}

// basic function that moves the enemy towards a passed in game object at a passed in speed and turns the enemy to face the object
void EnemyGameObject::moveToOtherGameObject(AbstractGameObject * gameObjectMovingTo, float moveSpeedX, float moveSpeedZ)
{
	// if the enemies collision box is coliding with the passed in objects bounding box
	// cease checking the rest (this is to prevent it from spinning around pointlessly when its on top of the 
	// player)
	if(!collisionBox->detectCollision(gameObjectMovingTo->collisionBox))
	{
		// if the enemys x is greater than the passed in objects position then take away from the enemies x position
		// else if the enemys x is less than the passed in objects position then add on to it
		if(position.x > gameObjectMovingTo->getPosition().x)
		{
			position.x -= moveSpeedX;
		}
		else if(position.x < gameObjectMovingTo->getPosition().x)
		{
			position.x += moveSpeedX;
		}
		
		// if the enemys z is greater than the passed in objects position then take away from the enemies z position
		// else if the enemys z is less than the passed in objects position then add on to it
		if(position.z > gameObjectMovingTo->getPosition().z)
		{
			position.z -= moveSpeedZ;
		}
		else if(position.z < gameObjectMovingTo->getPosition().z)
		{
			position.z += moveSpeedZ;
		}
	
		// calculating the angle of the player and changing it to degrees
		float check = (atan2(position.x-gameObjectMovingTo->getPosition().x, position.z-gameObjectMovingTo->getPosition().z))*57.29577951f;
		
		// setting it so that the enemy is facing the player(adding 180 on so that the character doesn't face away from the player)
		rotation.y = 180 + check;
	}
}