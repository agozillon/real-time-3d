#ifndef STATICLIGHT_H
#define STATICLIGHT_H
#include "AbstractLight.h" // including AbstractLight so the StaticLight can inherit from it and have access to its includes

// a class used to create StaticLights it inherits from the AbstractLight class
class StaticLight: public AbstractLight
{

public:
	//  StaticLight constructor that takes Amibent/Specular/Diffuse RGBAs, an initial position and scalar 
	StaticLight(float ambR, float ambG, float ambB, float ambA, float diffR, float diffG, float diffB, float diffA, float specR, float specG, float specB, float specA, float posX, float posY, float posZ, float scale);
	~StaticLight(){}																			//  blank inline destructor
	void updateAmbience(float r, float g, float b, float a);									//	function takes in floats as RGBA and is used to change the rgba values of the light structs ambience
	void updateDiffuse(float r, float g, float b, float a);										//  function takes in floats as RGBA and is used to change the rgba values of the light structs diffuse
	void updateSpecular(float r, float g, float b, float a);									//  function takes in floats as RGBA and is used to change the rgba values of the light structs specular
	void updateLightStructPositionAndScalar(float posX, float posY, float posZ, float scale);	//  function takes in floats as x/y/z/scalar and is used to change the light structs position and scalar
	void updateLightVectorPositionAndScalar(float posX, float posY, float posZ, float scale);	//  function takes in floats as x/y/z/scalar and is used to change the light vectors position and scalar
	void setLight(GLuint shaderID){rt3d::setLight(shaderID, light);}							//  inline function that takes in a shader and sets the light
	vec4 getLightStructAmbience();																//  function that returns the ambience as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructDiffuse();																//  function that returns the diffuse as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructSpecular();																//  function that returns the specular as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructPositionAndScalar();													    //  function that returns the struct position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	
	vec4 getLightVectorPositionAndScalar(){return lightPositionAndScalar;}						//  inline function that returns the vector position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	

private:
	// standard light structure and a vec4 to hold the light position and scalar holders
	rt3d::lightStruct light;
	vec4 lightPositionAndScalar;

};

#endif