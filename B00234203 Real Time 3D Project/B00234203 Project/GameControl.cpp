#include "GameControl.h"// including game header so that we can implement the functions, and including all of the various states so that I can instantiate them and create pointers to them
#include "StatePlay.h" 
#include "StateGameOver.h"
#include "StateGameWon.h" // for various states
#include "rt3d.h" // for exit function
#include <iostream> // for console output

// setting the instance pointer to start at null
GameControl * GameControl::instance = NULL;

GameControl::GameControl()
{
	playState = new StatePlay();
	gameOverState = new StateGameOver();
	gameWonState = new StateGameWon();
	currentState = playState;

   // creating a down casted pointer to playState for use in the exitPrompt state
   downCastedPlayState =  static_cast<StatePlay*>(playState);
}

// a function that returns an instance of Game class, it creates one if there isn't one already. but stops creation of anymore than one 
GameControl * GameControl::getInstance()
{
	if(!instance)
	{
		 instance = new GameControl();
	}

	return instance;
}

// returns the downcasted pointer to statePlay so other states can use various statePlay functions like save
StatePlay * GameControl::getCastedStatePlay(void)
{
	return downCastedPlayState;
}

// function that switches the current state to the new passed in state
void GameControl::setState(GameState* newState)
{
	if(newState != currentState) // if the new state is not the same as the current state change state
	{
		currentState = newState; // change the current State to the New state
		currentState->enter();
	}
}

// Set up rendering context
SDL_Window * GameControl::setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	
	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		std::cout << "TTF failed to initilize";

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);

	if (textFont == NULL)
		std::cout << "failed to open font";
	
	return window;
}

// function that holds the run time game loop 
void GameControl::run()
{ 
	hWindow = setupRC(glContext); // Create window and render context 
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << std::endl;
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;
	
	// calling the initilize of all the states
	// as if I don't then this can cause Memory Reference errors
	// when closing a state before going into it
	playState->init();
	gameOverState->init();
	gameWonState->init();

	running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	// the event loop
	{	
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
			{
				running = false;
			}
		}

		currentState->update(); // call the update function
		currentState->draw(); // call the draw function
	}
	
}

GameControl::~GameControl()
{
	delete playState;
	delete gameOverState;
	delete gameWonState;

	TTF_CloseFont(textFont);
    SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(hWindow); // destroying the current window
    SDL_Quit(); // quiting from SDL
}
