#include "ScoreItemGameObject.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms

// constructor that takes in position/rotation/scalar and mesh ID/IndexCount/texture ID and collisionBox positions and dimensions and instantiates/assigns them
ScoreItemGameObject::ScoreItemGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions)
{
	// assigning variables
	position = pos;
	scalar = scale;
	rotation = rot;
	isVisible = true;
	scoreIncrease = 100;

	// instantiating the BoundingBox by passing in the collisionBoxPos and collisionBoxDimensions
	collisionBox = new BoundingBox(collisionBoxPos, collisionBoxDimensions);

	// instantiating a new ObjModelHolder and passing in the Mesh/Index/Texture
	model = new ObjModelHolder(meshID, indexCount, textureID);
}

// destructor that deletes all objects instantiated within this class
ScoreItemGameObject::~ScoreItemGameObject()
{
	delete model;
	delete collisionBox;
}

// function that draws the ScoreItemGameObject by translating/rotating/scaling the matrix by the current position/rotation/scalar 
// and then calling the draw to draw the obj model after checking if the isVisible bool is true!
void ScoreItemGameObject::draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix)
{	
	if(isVisible == true)
	{
		mvMatrix = translate(mvMatrix, vec3(position.x, position.y, position.z));
		mvMatrix = rotate(mvMatrix, rotation.x, vec3(1.0f, 0.0f, 0.0f));
		mvMatrix = rotate(mvMatrix, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		mvMatrix = rotate(mvMatrix, rotation.z, vec3(0.0f, 0.0f, 1.0f));
		mvMatrix = scale(mvMatrix, vec3(scalar.x, scalar.y, scalar.z));
		model->draw(shaderID, material,  mvMatrix);
	}
}