#ifndef SCOREITEMGAMEOBJECT_H
#define SCOREITEMGAMEOBJECT_H
#include "AbstractGameObject.h" // including AbstractInGameObject so the ScoreItemGameObject can inherit from it and use it's includes 
#include "ObjModelHolder.h"     // including the ObjModelHolder class so that this class can implement it

class ScoreItemGameObject: public AbstractGameObject
{

public:
	// constructor that takes in the initial position/rotation/scalar of the InitialGameObject and takes in a meshID/indexcount/texutreID for the objModelHolder also takes in position and dimensions for a boundingbox
	ScoreItemGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions);
	~ScoreItemGameObject();															//  destructor that deletes this classes instantiated objects
	vec3 getPosition(){return position;}											//	inline function that returns the position as a vec3
	void updatePosition(vec3 pos){position = pos;}									//  inline function that takes a vec3 and updates the position
	vec3 getRotation(){return rotation;}											//	inline function that returns the rotation as a vec3			 
	void updateRotation(vec3 rot){rotation = rot;}									//	inline function that takes a vec3 and updates the rotation
	vec3 getScalar(){return scalar;}												//	inline function that returns the scalar as a vec3
	void updateScalar(vec3 scale){scalar = scale;}									//	inline function that takes a vec3 and updates the scalar
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);		//  function that takes a shader/material/matrix and uses them to draw the model
	int getScoreIncrease(){return scoreIncrease;}									//  inline function that returns the score increase value
	void updateIsVisible(bool change){isVisible = change;}						    //  inline function that updates the IsVisible boolean
	bool getIsVisible(){return isVisible;}											//  inline function that returns the IsVisible boolean
	
private:
	// vec3s for holding the position/rotation/scalar and an int for the scoreIncrease and a bool for checking if the item IsVisible
	vec3 position;
	vec3 rotation;
	vec3 scalar;
	int scoreIncrease;
	bool isVisible;

	// pointer to an ObjModelHolder type so ScoreItemGameObject can have an obj model
	ObjModelHolder * model;

};

#endif	
