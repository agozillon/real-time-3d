#ifndef GAMECONTROL_H
#define GAMECONTROL_H
#include <SDL_ttf.h>   // including SDL_ttf to set up the textFont
#include <SDL.h>	   // including SDL to set up the SDL window
#include "gameState.h" // including GameState so that I can create pointers and functions for the State objects
#include "statePlay.h"

//SIMILAR TO MY GED (B00234203) I Removed most of the stuff from the GED however down to a template of leaving it 
// as a singleton and having Runtime loop function(changed with the new runtime loop), setState function
//GetState, GetPlayState, getCastedStatePlay, and the getSDLWindow and setupRC functions(I changed the setupRC implementation from GED to the Real Time 3D
// one however). Anything with capital "USED" commented at the end of it's comment or next to it has been previously implemented


// this game class contains the run time loop and all of the instantiated states and functions to switch states
// it also has a few other functions and variables that need to be used over multiple states and/or classes
// it is currently a singleton this is largely to simplify changing states and using the various other functions 
// without having to pass in the Game object all of the time, it's also a singleton as we only ever want 1 instantiated 
// game object at a time. 
class GameControl
{

public:
	static GameControl *getInstance();                                     // function that returns a pointer to a Game Object so we can use the functions from here(static so that its useable from anywhere without an instansiated Game object)  USED
	void run();                                                            // function that contains the game run loop USED
	void setState(GameState * newState);                                   // function to change the current state to the passed in GameState USED
	GameState *getState(){return currentState;}                        // inline function returns the currentState GameState pointer USED
	GameState *getPlayState(){return playState;}					   // inline function returns the playState GameState pointer USED
	GameState * getGameOverState(){return gameOverState;}			   // inline function returns the gameOverState  GameState pointer USED
	GameState * getGameWonState(){return gameWonState;}			       // inline function returns the gameOverState  GameState pointer USED
	StatePlay *getCastedStatePlay();                                   // function that returns a pointer to PlayState tht has been casted to PlayState instead of GameState USED      
	SDL_Window * getSDLWindow(){return hWindow;}                           // inline function that returns the created SDL window USED
	~GameControl();                                                        // Game destructor 

private:
	GameControl();                                                         // private Game constructor so that no other class can accidently create an Game object (Singleton so there should only ever be one!)
	GameControl(GameControl const&){};                                     // creating my own copy constructor that is blank and private so that no copies can be created for my singleton USED
	SDL_Window * setupRC(SDL_GLContext &context);                          // creates an OpenGL context for use with SDL USED
	
	TTF_Font* textFont;													   // TTF_Font used to initalize and check that SDL_ttf is working on initilization USED
	
	static GameControl * instance;                                         // static Game pointer (for the singletons so there is only one of these) USED
	SDL_GLContext glContext;                                               // OpenGL context handle USED
	SDL_Window *hWindow;                                                    // SDL_Window type to hold the SDL_Window return from setupRC USED
      
	// GameState pointer for holding the various states USED
	GameState * currentState;                                              
	GameState * playState;                                                 
	GameState * gameOverState;
	GameState * gameWonState;											   
	StatePlay * downCastedPlayState;                                       // StatePlay pointer for pointing to StatePlay and using playState specific functions like load/save USED

	bool running;                                                          // USED

};

#endif