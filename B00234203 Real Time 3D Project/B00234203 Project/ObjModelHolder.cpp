#include "ObjModelHolder.h"

// constructor that takes in an indexcount, mesh ID and texture ID and sets them
ObjModelHolder::ObjModelHolder(GLuint meshID, GLuint indexCount, GLuint textureID)
{
	texture = textureID;
	meshIndexCount = indexCount;
	meshObject = meshID; 
}

// taking in a shader/material/matrix setting the material, binding the texture and setting the uniform and then drawing the obj model
void ObjModelHolder::draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(mvMatrix));
	rt3d::setMaterial(shaderID, material);
	rt3d::drawMesh(meshObject, meshIndexCount, GL_TRIANGLES);
}
