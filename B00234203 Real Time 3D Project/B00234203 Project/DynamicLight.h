#ifndef DYNAMICCLIGHT_H
#define DYNAMICCLIGHT_H
#include "AbstractLight.h" // including AbstractLight so this class can inherit and use the includes from AbstractLight

// DynamicLight class inherits from AbstractLight and takes max/min height and an initial position and an end traversal point
// on the X and then calculates a up slope and down slope for the light to traverse a triangular arc across the X and up the Y(doesn't do Z)
class DynamicLight: public AbstractLight
{

public:
	// dynamic light constructor that takes Amibent/Specular/Diffuse RGBAs, an initial position and scalar and an end point for the light to traverse back and forth from as well as a max and min height at which the light will descend/ascend to
	DynamicLight(float ambR, float ambG, float ambB, float ambA, float diffR, float diffG, float diffB, float diffA, float specR, float specG, float specB, float specA, float posX, float posY, float posZ, float scale, float endTravelX, float maxTravelH, float minTravelH);
	~DynamicLight(){}																				//  blank inline destructor
	void updateAmbience(float r, float g, float b, float a);									    //	function takes in floats as RGBA and is used to change the rgba values of the light structs ambience
	void updateDiffuse(float r, float g, float b, float a);											//  function takes in floats as RGBA and is used to change the rgba values of the light structs diffuse
	void updateSpecular(float r, float g, float b, float a);										//  function takes in floats as RGBA and is used to change the rgba values of the light structs specular
	void updateLightStructPositionAndScalar(float posX, float posY, float posZ, float scale);		//  function takes in floats as x/y/z/scalar and is used to change the light structs position and scalar
	void updateLightVectorPositionAndScalar(float posX, float posY, float posZ, float scale);		//  function takes in floats as x/y/z/scalar and is used to change the light vectors position and scalar
	void setLight(GLuint shaderID){rt3d::setLight(shaderID, light);}								//  inline function that takes in a shader and sets the light
	vec4 getLightStructAmbience();														     		//  function that returns the ambience as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructDiffuse();															    	//  function that returns the diffuse as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructSpecular();															    	//  function that returns the specular as a vec4 (largely because returning them all singularrly would be overkill)
	vec4 getLightStructPositionAndScalar();													        //  function that returns the struct position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	
	vec4 getLightVectorPositionAndScalar(){return lightPositionAndScalar;}						    //  inline function that returns the vector position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	
	float getStartTravelPointX(){return startTravelPointX;}											//  inline function that returns the starting point on the X regardless of the lights current position
	float getEndTravelPointX(){return endTravelPointX;}												//  inline function that returns the end travelling point position on the X axis (the point its travelling to)
	float getMidTravelPointX(){return midTravelPointX;}												//  inline function that returns the mid travelling point position on the X axis
	float getMaxTravelHeight(){return maxTravelHeight;}												//  inline function that returns the maximum travelling height on the Y that the light will travel too
	float getMinTravelHeight(){return minTravelHeight;}												//  inline function that returns the minimum travelling height on the Y that the light will travel too
	void updateStartTravelPointX(float posX);														//  function that takes in a new start travelling point for the light and then updates the slope values/ mid point values accordingly
	void updateEndTravelPointX(float posX);															//  function that takes in a new end travelling point for the light and then updates the slope values/ mid point values accordingly
	void updateMaxTravelHeight(float posY);															//  function that takes in a new maximum travelling height for the light and then updates the slope values/ mid point values accordingly
	void updateMinTravelHeight(float posY);															//  function that takes in a new minimum travelling height for the light and then updates the slope values/ mid point values accordingly
	void moveDynamicLight();																		//  function that moves the light by the slope values when called until the end point and then reverses them back to the start point


private:
	// private function that calculates the mid point between two passed in points and then returns them
	float calculateMidPoint(float firstPoint, float secondPoint);

	// private function that calculates a slope from 2 sets of X/Y positions
	float calculateSlope(float firstPosX, float firstPosY, float secondPosX, float secondPosY);

	// standard light structure and a vec4 to hold the light position and scalar holders
	rt3d::lightStruct light;
	vec4 lightPositionAndScalar; 

	//variables that the dynamic light needs to calculate its path and move
	// specifying that the travel points are on the X as they could be misinterpreted to be on the Z)
	float startTravelPointX;
	float midTravelPointX;
	float endTravelPointX;
	float maxTravelHeight;
	float minTravelHeight;
	bool lightReverseDirection;

	// slope values that will be used to increment the light upwards/downwards depending on the lights current direction 
	float startMidSlopeValue;
	float midEndSlopeValue;

};

#endif