#ifndef MD2MODELHOLDER_H
#define MD2MODELHOLDER_H
#include "AbstractModelHolder.h" // includes AbstractModelHolder so the class can inherit from it and use its includes 
#include "md2model.h"			 // includes Md2Model so this class can have an md2Model type and use its functions

// class that is used to hold an md2model and its variables and also load it in as I currently do not 
// have a MD2 extension for the ObjModelAndTexture handler (hence the class name), it also has a very basic draw function that
// gets called within the GameObject classes it sets materials etc but does no matrix manipulations
class MD2ModelHolder: public AbstractModelHolder 
{

public:
	MD2ModelHolder(char *fileName, GLuint textureID);				                // consructor that takes in an md2 model file name and loads it in and it also takes in a textureID for the model 
	~MD2ModelHolder();																// basic destructor that deletes objects instantiated
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);		// function takes in a shader ID a material structure and a passed in matrix to draw the md2model
	void animateAndUpdateModel();											        // function that animates and updates the md2 model
	int getCurrentAnimation(){return currentAnimation;}					            // function that returns the models currentAnimation as an integer 
	void updateCurrentAnimation(int anim){currentAnimation = anim;}		            // function that updates the currentAnimation by a passed in integer

private:
	// md2model pointer to hold an md2model so we can use its various funtions
    md2model * model;

	// GLuint to store the loaded md2models variables and an int to hold the currentAnimation
	GLuint texture;
	GLuint meshObject;
	GLuint meshIndexCount;
	int currentAnimation;

};

#endif