#include "SkyBox.h"
#include <stack>      // included for stack container

// passing in the textures and setting up the number of walls and then creating the BasicQuads for the SkyBox
SkyBox::SkyBox(GLuint topTextureID, GLuint leftTextureID, GLuint rightTextureID, GLuint backTextureID, GLuint frontTextureID)
{
	texture[0] = topTextureID;
	texture[1] = leftTextureID;
	texture[2] = rightTextureID;
	texture[3] = backTextureID;
	texture[4] = frontTextureID;

	// constructing the BasicQuads with a default size of 1 width/height
	textureQuad = new BasicQuad(1, 1);
}

// function for drawing the SkyBox takes in a shaderID and a matrix
void SkyBox::draw(GLuint shaderID, mat4 mvMatrix)
{
	// turning depth mask off to draw the skybox so that for the duration of
	// the SkyBox it doesn't check if its behind or infront of other models
	glDepthMask(GL_FALSE);
	
	std::stack<mat4> modelStack; 

	// drawing top of SkyBox by binding the texture, rotating/positioning/scaling it by set values
	// setting the uniform matrix and drawing the BasicQuad
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	modelStack.push(mvMatrix);
	modelStack.top() = scale(modelStack.top(), vec3(2.0f, 2.0f, 2.0f));
	modelStack.top() = translate(modelStack.top(), vec3(0.0f, 2.0f, 0.0f));
	modelStack.top() = rotate(modelStack.top(), 90.0f, vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	
	// drawing left of skyBox by binding the texture, rotating/positioning/scaling it by set values
	// setting the uniform matrix and drawing the BasicQuad
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	modelStack.push(mvMatrix);
	modelStack.top() = scale(modelStack.top(), vec3(2.0f, 2.0f, 2.0f));
	modelStack.top() = translate(modelStack.top(), vec3(-2.0f, 0.0f, 0.0f));
	modelStack.top() = rotate(modelStack.top(), 90.0f, vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	
	// drawing right of SkyBox by binding the texture, rotating/positioning/scaling it by set values
	// setting the uniform matrix and drawing the BasicQuad
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	modelStack.push(mvMatrix);
	modelStack.top() = scale(modelStack.top(), vec3(2.0f, 2.0f, 2.0f));
	modelStack.top() = translate(modelStack.top(), vec3(2.0f, 0.0f, 0.0f));
	modelStack.top() = rotate(modelStack.top(), -90.0f, vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	
	// drawing back of SkyBox by binding the texture, rotating/positioning/scaling it by set values
	// setting the uniform matrix and drawing the BasicQuad
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	modelStack.push(mvMatrix);
	modelStack.top() = scale(modelStack.top(), vec3(2.0f, 2.0f, 2.0f));
	modelStack.top() = translate(modelStack.top(), vec3(0.0f, 0.0f, 2.0f));
	modelStack.top() = rotate(modelStack.top(), 180.0f, vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	// drawing front of SkyBox by binding the texture, rotating/positioning/scaling it by set values
	// setting the uniform matrix and drawing the BasicQuad
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	modelStack.push(mvMatrix);
	modelStack.top() = scale(modelStack.top(), vec3(2.0f, 2.0f, 2.0f));
	modelStack.top() = translate(modelStack.top(), vec3(0.0f, 0.0f, -2.0f));
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	// turning depth mask back on
	glDepthMask(GL_TRUE);
}

// destructor for deleting the objects instantiated inside the SkyBox
SkyBox::~SkyBox()
{
	   delete textureQuad;
}
