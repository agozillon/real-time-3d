#ifndef STATEGAMEWON_H
#define STATEGAMEWON_H
#include "GameState.h"					// including gameState so stateGameOver can inherit from it 
#include "BasicGameObject.h"			// including BasicGameObject for the GameOverScreen
#include "StaticLight.h"                // including StaticLight to use light within this state
#include "Material.h"					// including Material to use a material within this state
#include "ObjModelAndTexutreHandler.h"  // including ObjModelAndTextureHandler to use within this state
#include "FirstPersonCamera.h"			// including FirstPersonCamera to use within this state 
#include <glm/gtc/type_ptr.hpp>         // including type_ptr to use glm types
using namespace glm;					// (no real need for it here as it gets it from includes, just specifying programmer intent) using glm name space so I don't have to include variablename

// SIMILAR CONCEPT TO THE GED except I deleted everything in it to start
// from a blank state template with just Constructor/Deconstructor, void draw/update/init/entry functions

// as you can see below StateGameWon is a culmination of all of the inherited Abstract classes functions
class StateGameWon: public GameState
{

public:
	StateGameWon(){}                                   // StateGameWon blank inline constructor
	void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void draw();                                       // Function that is used to draw collections of other objects during this state
    void update();                                     // Function that is used to update this state                                  
	void enter();									   // function used to set certain things upon entry to this state
	~StateGameWon();                                   // StateGameWon destructor

private:
	// GLuint to hold the shader ID
	GLuint shaderProgram;
	
	// setting up various object pointers
	StaticLight * light;
	FirstPersonCamera * camera;
	Material * material;
	ObjModelAndTextureHandler * objModelAndTextureHandler;
	BasicGameObject * gameWonScreen;

};

#endif