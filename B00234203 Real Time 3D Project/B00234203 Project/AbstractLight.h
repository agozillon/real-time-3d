#ifndef ABSTRACTLIGHT_H
#define ABSTRACTLIGHT_H
#include "rt3d.h"				 // including rt3d library so that derived do not need to include it
#include <glm/gtc/type_ptr.hpp>  // including glm/gtc/type_ptr so that this class can use vec4s
using namespace glm;			 // using glm name space so I don't have to include variablename

// Abstract light class that all lights inherit from it specificys that they require functions to set/get 
//position/ambience/diffuse/specular variables as well as have a function to set the light  
class AbstractLight
{

public:
	virtual ~AbstractLight(){}																				// virtual inline destructor 
	virtual void updateAmbience(float r, float g, float b, float a) = 0;									// virtual function that should be implemented to change the rgba values of the light structs ambience
	virtual void updateDiffuse(float r, float g, float b, float a) = 0;									    // virtual function that should be implemented to change the rgba values of the light structs diffuse
	virtual void updateSpecular(float r, float g, float b, float a) = 0;									// virtual function that should be implemented to change the rgba values of the light structs specular
	virtual void updateLightStructPositionAndScalar(float posX, float posY, float posZ, float scale) = 0;   // virtual function that should be implemented to change the light structs pos and scalar
	virtual void updateLightVectorPositionAndScalar(float posX, float posY, float posZ, float scale) = 0;	// virtual function that should be implemented to change the light vectors position and scalar
	virtual void setLight(GLuint shaderID) = 0;																// virtual fucntion that should be implemented to take in a shader and set the light
	virtual vec4 getLightStructAmbience() = 0;															    // virtual function that should be implemented to return the ambience as a vec4 (largely because returning them all singularrly would be overkill)
	virtual vec4 getLightStructDiffuse() = 0;															    // virtual function that should be implemented to return the diffuse as a vec4 (largely because returning them all singularrly would be overkill)
	virtual vec4 getLightStructSpecular() = 0;															    // virtual function that should be implemented to return the specular as a vec4 (largely because returning them all singularrly would be overkill)
	virtual vec4 getLightStructPositionAndScalar() = 0;												        // virtual function that should be implemented to return the struct position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	
	virtual vec4 getLightVectorPositionAndScalar() = 0;												        // virtual function that should be implemented to return the vector position and scalar as a vec4 (largely because returning them all singularrly would be overkill)	

};

#endif