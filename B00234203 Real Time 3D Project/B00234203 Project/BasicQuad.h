#ifndef BASICQUAD_H
#define BASICQUAD_H
#include "rt3d.h"	 // including rt3d library for the create a mesh function
#include <GL/glew.h> // including glew for GLuints

// really basic quad class, used in the creation of the Skybox and PlayerHUD classes I chose to use this
// instead of loading in a quad model as it's simpler and allows the programmer more freedom than hard coding it into
// the hud or skybox
class BasicQuad
{

public:
	BasicQuad(float h, float w);				 // constructor takes in a height and width to create the quad to these sizes
	~BasicQuad(){}								 // inline blank destructor 
	void draw();								 // function that can be called to draw the quad
	float getHeight(){return height;}            // returns the height of the quad
	float getWidth(){return width;}				 // returns the width of the quad, I have no updates as I can't resize the quad after creation and do not wish to scale it here 

private:
	// array of vertices to help create the Quad, meshobject/vertexcount/quadindexcount to hold values and the height/width of the quad
	GLfloat quadVertices[12];
	GLuint meshObject;
	GLuint vertexCount;
	GLuint indexCount;
	float height;
	float width;

};

#endif
