#ifndef MATERIAL_H
#define MATERIAL_H
#include "rt3d.h"				// including rt3d so we can use rt3d functions 
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so we can use glm types
using namespace glm;			// using glm name space so I don't have to include variablename

// material class for encapsulating the material structure
class Material
{

public:
	// material constructor that accepts RGBA ambient/diffuse/specular values as floats as well as a shine value to create the material struct
	Material(float ambR, float ambG, float ambB, float ambA, float diffR, float diffG, float diffB, float diffA, float specR, float specG, float specB, float specA, float shine);
	~Material(){}																// basic blank inline destructor
	void updateAmbience(float r, float g, float b, float a);					// function takes in floats as RGBA and is used to change the rgba values of the material structs ambience
	void updateDiffuse(float r, float g, float b, float a);						// function takes in floats as RGBA and is used to change the rgba values of the material structs diffuse
	void updateSpecular(float r, float g, float b, float a);					// function takes in floats as RGBA and is used to change the rgba values of the material structs specular
	void updateShininess(float shine){material.shininess = shine;}				// inline function takes in a float as a shine value and is used to change the value of the material structs shine
	rt3d::materialStruct getFullMaterialStruct(){return material;}				// inline function that returns the material as an rt3d material structure
	vec4 getMaterialStructAmbience();									    	// function that returns the material structs ambience as a vec4
	vec4 getMaterialStructDiffuse();										    // function that returns the material structs diffuse as a vec4
	vec4 getMaterialStructSpecular();										    // function that returns the material structs specular as a vec4
	float getMaterialStructShininess(){return material.shininess;}				// inline function that returns the material structs shine value as a float

private:
	// basic rt3d material struct for holding all of the values
	rt3d::materialStruct material; 

};

#endif