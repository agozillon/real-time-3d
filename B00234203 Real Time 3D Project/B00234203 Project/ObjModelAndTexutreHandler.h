#ifndef OBJMODELANDTEXTUREHANDLER_H
#define OBJMODELANDTEXTUREHANDLER_H
#include "rt3dObjLoader.h"				// including the rt3dObjLoader so that I can load in Obj Models 
#include "rt3d.h"						// including the rt3d library for its functionallity
#include <vector>                       // including the vector class so I can use vectors
#include <GL/glew.h>					// including glew so I can use GLuints

// class used to load in and store Obj models and textures so that they only need to be loaded once
class ObjModelAndTextureHandler
{

public:
	ObjModelAndTextureHandler(){}                                                         // blank inline constructor
	~ObjModelAndTextureHandler(){}														  // blank inline destructor
	void loadAndStoreObjModel(char *fileName);											  // function that takes a model name and loads in the model and then stores it within the correct vector stacks
	void loadAndStoreTexture(char *fileName, bool clampToEdge);							  // function that takes a texture name and a boolean and loads in the texture clamped or unclamped to edge depending on the bool and then stores it within the correct vector stacks
	void getObjModel(GLuint &meshID, GLuint &indexCount, int posInVector);				  // function that takes in two GLuints as a reference and an integer representing the position of the wanted obj model in the vector and then changes the passed in GLuints and sets them to the wanted obj model ID/vectorCount
	int getNumberOfObjModelsStored(){return objMeshObjectList.size();}					  // function that returns the current number of obj models stored as an integer
	void getTextureID(GLuint &textureID, int posInVector);								  // function that takes in a GLuint as a reference and an integer representing the position of the wanted texture in the vector and then changes the passed in GLuint and sets it to the wanted texture ID 
	int getNumberOfTexturesStored(){return textureIDList.size();}						  // function that returns the number of textures store as an integer

private:
	// vector's of GLuints to hold the loaded in objects MeshID and IndexCounts as well as the texturesID
	std::vector<GLuint> objMeshIndexCountList;
	std::vector<GLuint> objMeshObjectList;
	std::vector<GLuint> textureIDList;

};

#endif