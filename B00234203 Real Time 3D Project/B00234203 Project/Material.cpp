#include "Material.h"

// constructor that takes in the Material values and then sets up the values by using the update function
Material::Material(float ambR, float ambG, float ambB, float ambA, float diffR, float diffG, float diffB, float diffA, float specR, float specG, float specB, float specA, float shine)
{
	updateAmbience(ambR, ambG, ambB, ambA);
	updateDiffuse(diffR, diffG, diffB, diffA);
	updateSpecular(specR, specG, specB, specA);
	updateShininess(shine);
}

// function that takes in floats to update the Material ambience
void Material::updateAmbience(float r, float g, float b, float a)
{
	material.ambient[0] = r;
	material.ambient[1] = g;
	material.ambient[2] = b;
	material.ambient[3] = a;
}

// function that takes in floats to update the Material diffuse
void Material::updateDiffuse(float r, float g, float b, float a)
{
	material.diffuse[0] = r;
	material.diffuse[1] = g;
	material.diffuse[2] = b;
	material.diffuse[3] = a;
}

// function that takes in floats to update the Material specular
void Material::updateSpecular(float r, float g, float b, float a)
{
	material.specular[0] = r;
	material.specular[1] = g;
	material.specular[2] = b;
	material.specular[3] = a;
}

// function that returns the material structures ambience as a vec4
vec4 Material::getMaterialStructAmbience()
{
	return vec4(material.ambient[0], material.ambient[1], material.ambient[2], material.ambient[3]);
}

// function that returns the material structures diffuse as a vec4
vec4 Material::getMaterialStructDiffuse()
{
	return vec4(material.diffuse[0], material.diffuse[1], material.diffuse[2], material.diffuse[3]);
}

// function that returns the material structures specular as a vec4 
vec4 Material::getMaterialStructSpecular()
{
	return vec4(material.specular[0], material.specular[1], material.specular[2], material.specular[3]);
}
