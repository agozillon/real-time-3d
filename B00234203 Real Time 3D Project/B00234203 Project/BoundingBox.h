#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H
#include "rt3d.h"				// including rt3d library so that this class can use rt3d functionallity
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
using namespace glm;			// using glm name space so I don't have to include variablename


// basic bounding box class, takes in dimensions/position and has functions to check for collisions
// also can be used to draw the bounding box to visually represent it
class BoundingBox
{

public:
	BoundingBox(vec3 pos, vec3 bDimensions);						  // constructor that takes in the position and dimensions of the bounding box (dimensions paramater named bDimensions as I couldn't name it dimensions since dimensions is a variable within boundingbox) 
	~BoundingBox(){}												  // blank inline destructor
	void drawVisibleBoundingBox(GLuint shaderID, mat4 mvMatrix);      // function that takes in a shader and a matrix and then draws the bounding box to screen not used in the final project but useful none the less
	vec3 getDimensions(){return dimensions;}						  // function that returns the bounding boxes current dimensions as a vec3
	vec3 getPosition(){return position;}							  // inline function that returns the bounding boxes position as a vec3
	vec3 getColliderPosition(){return colliderPosition;}			  // inline function that returns the colliders position before collision as a vec3, in this case the player class feeds its position in and this returns its last one not the best way but it works 
	void updateColliderPosition(vec3 pos){colliderPosition = pos;}    // inline function that sets the collider(players) position before collision 
	void updatePosition(vec3 pos){position = pos;}					  // inline function that updates the bounding boxs position using vec3s
	void createVisibleBoundingBox();								  // function that creates a mesh of the bounding boxs current position and dimensions, used to help postion the bounding boxes not used within the final project
	bool detectCollision(BoundingBox * colideeBoundingBox);			  // function that takes in a BoundingBox pointer and checks if there is a collision between the current bbox and the passed in bbox

private:
	// used for the creating the visual representation of the boundingbox 
	GLfloat cubeVertices[24];
	GLuint meshObject;
	GLuint vertexCount;
	GLuint cubeIndexCount;
	
	// vec3s for holding the basic variables this class requires
	vec3 dimensions;	
	vec3 position;
	vec3 colliderPosition;

};

#endif
