#ifndef BASSAUDIOLOADER_H
#define BASSAUDIOLOADER_H
#include "bass.h"  // including the bass library so that I can load and manipulate audio files

// extremely simple audio holding class, created using bass it takes in an audio file name, loads it and then stores it for playing
class BassAudioLoader
{

public:
	BassAudioLoader(char *fileName);       // basic constructor that accepts the audio files name and loads it in
	~BassAudioLoader(){}				   // inline blank destructor						
	void playAudio();					   // function used to play the audio file
	void pauseAudio(){BASS_Pause();}	   // inline function used to pause the audio file
	void resumeAudio(){BASS_Start();}	   // inline function used to resume the audio file once paused

private:
	// private function that loads in the file and returns it as a HSAMPLE once the constructor has passed it the name
	HSAMPLE loadSample(char * fileName);

	// HSAMPLE for holding the audio file once loaded
	HSAMPLE sample;

};

#endif