#include "StateGameOver.h"
#include "GameControl.h"
#include <stack>  // including for access to the stack container

// function for calling all the things required to initilize this state
void StateGameOver::init()
{
	// initiating shaders and making sure the correct shader is in use
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	glUseProgram(shaderProgram);

	// initializing a new material class and setting various RGBA values for ambience/diffuse/specular/shine
	material = new Material(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.0f);
	
	// initiating the light and passing in the shader and various RGBA values for ambience/diffuse/specular/position/scale
	//the scene lighting it is static in this case and not dynamic like state play
	light = new StaticLight(0.3f, 0.3f, 0.3f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -10.0f, 10.0f, 20.0f, 1.0f);
	light->setLight(shaderProgram);
	
	// setting up a object model and texture handler for this class and loading in various textures and obj models
	objModelAndTextureHandler = new ObjModelAndTextureHandler();
	objModelAndTextureHandler->loadAndStoreObjModel("cube.obj");					// 0th obj model
	objModelAndTextureHandler->loadAndStoreTexture("gameoverimage.bmp", true);    // 0th texture
	
	// setting up temporary GLuint's to get and temporarily hold the obj model and texture IDs
	GLuint tempTextureID;
	GLuint tempMeshID;
	GLuint tempIndexCount;

	// getting the chosen texture and model ID's from the sceneModel and texture handler 
	// and then passing them into the instantiated BasicInGameObject alongside its variables
	// position/rotation/scale/mesh/index/texture/collision box(set at 0,0,0)
	objModelAndTextureHandler->getTextureID(tempTextureID, 0);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 0);
	gameOverScreen = new BasicGameObject(vec3(-0.5f, 1.5f, -1.65f), vec3(0.0f, 90.0f, 180.0f), vec3(1.0f, 1.0f, 1.0f), tempMeshID, tempIndexCount, tempTextureID, vec3(0, 0, 0), vec3(0, 0, 0));
	
	// instantiating the scene camera which will be a first person camera
	camera = new FirstPersonCamera();

	// enabling depth test/blend/cullface and setting them up
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LESS);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
 
// function for calling all the things required to draw this state
void StateGameOver::draw()
{
	// setting the clear color and clearing the buffer
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	
	// setting up the projection matrix for this state fov/aspect ratio/near plane/far plane and then setting projection uniform
	mat4 projection(1.0);
	projection = perspective(60.0f, 800.0f/600.0f, 0.5f, 200.0f);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", value_ptr(projection));
	
	// creating a stack to hold the model view, then pushing an identity matrix and then getting
	// the cameras eye/up/at
	std::stack<mat4> mvStack; 
	mvStack.push(mat4(1.0));
	mvStack.top() = camera->getCurrentCamera();

	// moves and updates the lights position in accordance to the camera position and then setting it
	vec4 temp = mvStack.top()*light->getLightVectorPositionAndScalar();
	light->updateLightStructPositionAndScalar(temp.x, temp.y, temp.z, light->getLightStructPositionAndScalar().w);
	light->setLight(shaderProgram);
	
	// passing in the shader/material/stack and drawing the gameOverScreen
	gameOverScreen->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	mvStack.pop();


	SDL_GL_SwapWindow(GameControl::getInstance()->getSDLWindow()); // swap buffers
}
 
// update class for updating various things within the current state
// during run time
void StateGameOver::update()
{
	// unsigned 8 bit integer pointer to SDL keyboard state to detect which keys are being pressed
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	// when return is pressed switch the state back to the play state
	if (keys[SDL_SCANCODE_RETURN])
	{
		GameControl::getInstance()->setState(GameControl::getInstance()->getPlayState());
	}
}

// function that normally initiates variables etc. on entry to the state
// but is empty in this case
void StateGameOver::enter()
{
}

// destructor deleting all the objects instantiated within this class
StateGameOver::~StateGameOver()
{
	delete light;
	delete camera;
	delete gameOverScreen;
	delete objModelAndTextureHandler;
	delete material;
}
