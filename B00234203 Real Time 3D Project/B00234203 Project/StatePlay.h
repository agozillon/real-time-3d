#ifndef STATEPLAY_H
#define STATEPLAY_H
#include "GameState.h"					   // including gameState so statePlay can inherit from it 
#include "BasicGameObject.h"			   // including my various classes so I can use them within this state
#include "ScoreItemGameObject.h"
#include "EnemyGameObject.h"
#include "SkyBox.h"
#include "DynamicLight.h"
#include "ThirdPersonCamera.h"
#include "Material.h"
#include "BassAudioLoader.h"
#include "PlayerHUD.h"
#include "PlayerGameObject.h"
#include "ObjModelAndTexutreHandler.h"
#include <glm/glm.hpp>					   // including various glms to use vec3s etc within this scene
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "rt3d.h"						   // including rt3d namespace to use various functions from it
#include <ctime>                           // including ctime so that I can use various time functions
using namespace glm;					   // (no real need for it here as it gets it from includes, just specifying programmer intent) using glm name space so I don't have to include glm::variablename

// SIMILAR CONCEPT TO THE GED except I deleted everything in it to start
// from a blank state template with just Constructor/Deconstructor, void draw/update/init/entry functions

// as you can see below StatePlay is a culmination of all of the inherited Abstract classes functions
class StatePlay: public GameState
{

public:
	StatePlay(){}                                      // StatePlay blank inline constructor
	void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void draw();                                       // Function that is used to draw collections of other objects during this state
    void update();                                     // Function that is used to update this state                                 
	void enter();									   // function used to set certain things upon entry to this state
	~StatePlay();                                      // StatePlay destructor

private:

	// GLuints to hold the shader IDs
	GLuint shaderProgram;
	GLuint skyBoxProgram;
	
	// int's to hold the amount of each object there is within the class and the maximumScore for this state
	int numberOfWalls;
	int numberOfBarriers;
	int numberOfBuildings;
	int numberOfPickups;
	int numberOfSoundEffects;
	int numberOfEnemies;
	int maximumScore;

	// a bool to switch the skyboxs between my version and lab version and one to turn the dynamicLights movement on and off
	bool dynamicLightMovementOn;
	
	// a bool to check if the player is moving (w/s/a/d pressed down)
	bool playerMoving;

	// clock_t variables to store the currentTime and store the time a player collided with an enemy and another for the players temporary invincibility
	clock_t currentTime;
	clock_t enemyCollisionTime;
	clock_t tempInvincibility;

	// pointers to various object types that will be instantiated within this state
	PlayerGameObject * playerCharacter;
	ObjModelAndTextureHandler * objModelAndTextureHandler;
	DynamicLight * light;
	Material * material;
	ThirdPersonCamera * camera;
	SkyBox * skyBox;
	BasicGameObject * ground;
	BasicGameObject * wall[5];
	BasicGameObject * barrier[3];
	BasicGameObject * building[2];
	ScoreItemGameObject * pickups[3];
	EnemyGameObject * enemy[3];
	BassAudioLoader * soundEffect[2];
};

#endif