#ifndef OBJMODELHOLDER_H
#define OBJMODELHOLDER_H
#include "AbstractModelHolder.h" // including AbstractModelHolder class so the ObjModelHolder can inherit from it and use its includes


// this class inherits from AbstractModelHolder and is used to hold an obj model and 
// it also has a very basic draw function that gets called within the GameObject classes it sets materials etc 
// but does no matrix manipulations(maybe not ideal but it means less to be stored in GameObject classes)
class ObjModelHolder: public AbstractModelHolder 
{

public:
	ObjModelHolder(GLuint meshID, GLuint indexCount, GLuint textureID);          // constructor that takes in GLuints as MeshIDs/indexCount/TextureIDs and assigns them to the class GLuints
	~ObjModelHolder(){}															 // blank inline destructor
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);    // function takes in a shader ID a material structure and a passed in matrix to draw the obj model

private:
	// GLuints to hold the mesh, texture and index count
    GLuint meshObject;
	GLuint texture;
	GLuint meshIndexCount;

};

#endif