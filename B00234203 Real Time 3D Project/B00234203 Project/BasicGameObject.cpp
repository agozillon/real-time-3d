#include "BasicGameObject.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms

// constructor that takes in position/rotation/scalar and mesh ID/IndexCount/texture ID and collisionBox positions and dimensions and instantiates/assigns them
BasicGameObject::BasicGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions)
{
	// setting up the position/scalar/rotation
	position = pos;
	scalar = scale;
	rotation = rot;

	// passing in values to create the collision box for this object
	collisionBox = new BoundingBox(collisionBoxPos, collisionBoxDimensions);                     

	// passing in the meshID/indexCount and textureID to create the ObjModelHolder for storage and later use
	model = new ObjModelHolder(meshID, indexCount, textureID);
}

// destructor deleting objects instantiated within this class
BasicGameObject::~BasicGameObject()
{
	delete model;
	delete collisionBox;
}

// drawing the BasicGameObject scaling/rotaing/translating the matrix by the current position/rotation/scalar 
// and then calling the draw to draw the obj model
void BasicGameObject::draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix)
{	
	mvMatrix = translate(mvMatrix, vec3(position.x, position.y, position.z));
	mvMatrix = rotate(mvMatrix, rotation.x, vec3(1.0f, 0.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.y, vec3(0.0f, 1.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.z, vec3(0.0f, 0.0f, 1.0f));
	mvMatrix = scale(mvMatrix, vec3(scalar.x, scalar.y, scalar.z));
	model->draw(shaderID, material, mvMatrix);
}