// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "GameControl.h"

// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) 
{
 
		// creating a pointer to a Game Object 
    GameControl *newGame; 

	// getting an Instance of game and setting newGame to point to it
	newGame = GameControl::getInstance(); 
	
	// calling the game run function 
	newGame->run(); 
	
	// deleting new game and returning 0 to say that it was a success
	delete newGame;

    return 0;
}