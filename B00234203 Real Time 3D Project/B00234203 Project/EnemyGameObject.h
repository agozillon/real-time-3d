#ifndef ENEMYGAMEOBJECT_H
#define ENEMYGAMEOBJECT_H
#include "AbstractGameObject.h" // including AbstractGameObject so that the EnemyGameObject can inherit from it and get access to RT3D and the GLM types
#include "ObjModelHolder.h"     // including ObjModelHolder so that the enemy can have an ObjModelHolder object  

// The EnemyGameObject class is a slightly more advanced GameObject class
// that uses 2 bounding boxes one for Agression range and another for normal
// collision it also has a moveToOtherGameObject function which moves the 
// EnemyGameObject towards another GameObject at an input speed
// the enemy model also rotates towards the passed in GameObject  
// also uses an Obj model not an MD2 model
class EnemyGameObject: public AbstractGameObject
{
	
public:
	// constructor that takes in the initial position/rotation/scalar of the EnemyGameObject and takes in a meshID/indexcount/texutreID for the objModelHolder also takes in position and dimensions for 2 boundingboxes a collisionBox and an agroBox	
	EnemyGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions, vec3 agroBoxPos, vec3 agroBoxDimensions);
	~EnemyGameObject();																						 // class destructor
	vec3 getPosition(){return position;}																     // inline function that returns the position as a vec3
	void updatePosition(vec3 pos){position = pos;}												             // inline function that takes a vec3 and updates the position
	vec3 getRotation(){return rotation;}																     // inline function that returns the rotation as a vec3			 
	void updateRotation(vec3 rot){rotation = rot;}									   					     // inline function that takes a vec3 and updates the rotation
	vec3 getScalar(){return scalar;}																	     // inline function that returns the scalar as a vec3
	void updateScalar(vec3 scale){scalar = scale;}														     // inline function that takes a vec3 and updates the scalar
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);							     // function that takes a shader/material/matrix and uses them to draw the model
	void moveToOtherGameObject(AbstractGameObject * gameObjectMovingTo, float moveSpeedX, float moveSpeedZ); // function that takes in a pointer to an AbstractGameObject(or derived) and also takes in a speed on the x and y and moves the enemy towards the passed in GameObjects position

	BoundingBox * agroBox;																					 // pointer to a second bounding box called agroBox

private:
	// variables required to position/rotate/scale the enemy 
	vec3 position;
	vec3 rotation;
	vec3 scalar;

	// pointer to an ObjModelHolder
	ObjModelHolder * model;
	
};

#endif	
