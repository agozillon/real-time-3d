#include "FirstPersonCamera.h"


// constructor that sets default values up for the FirstPersonCamera
FirstPersonCamera::FirstPersonCamera()
{
	// calling the update functions to make the constructor shorter
	updateEye(vec3(0.0, 1.0, 0.0));
	updateAt(vec3(0.0, 1.0, -1.0));
	updateUp(vec3(0.0, 1.0, 0.0));

	rotation = 0;
}

// moves the camera forward or backwards using a passed in distance
// for the eye and using the eye value and a defualt 1 value to move the at
// so the camera is always looking a set distance ahead
void FirstPersonCamera::moveForwardOrBackwards(float dist)
{
	eye = rt3d::moveForward(eye, rotation, dist);
	at = rt3d::moveForward(eye, rotation, 1.0f);
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveRightOrLeft(float dist)
{
	eye = rt3d::moveRight(eye, rotation, dist);
	at = rt3d::moveForward(eye, rotation, 1.0f);
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveUpOrDown(float dist)
{
	eye = rt3d::moveUp(eye, rotation, dist);
	at = rt3d::moveForward(eye, rotation, 1.0f);
}

// rotates the camera right or left by rotating the position the camera is staring
// at via the moveForward function and a passed in rotation value
void FirstPersonCamera::updateRotation(float rot)
{
	rotation += rot;
	at = rt3d::moveForward(eye, rotation, 1.0f);
}
