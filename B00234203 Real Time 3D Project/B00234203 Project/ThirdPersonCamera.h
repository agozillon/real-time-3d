#ifndef THIRDPERSONCAMERA_H
#define THIRDPERSONCAMERA_H
#include "AbstractCamera.h"  // include AbstractCamera so that ThirdPersonCamera can inherit and get access to its includes 
#include <glm/gtc/matrix_transform.hpp> // included for lookAt

// basic ThirdPersonCamera class that inherits from AbstractCamera and can be used to Look at an object and follow the 
// object around the screen
class ThirdPersonCamera: public AbstractCamera
{

public:
	// constructor that takes in the position the camera is to lookAt and the follow distance from the object and the cameras original rotation
	ThirdPersonCamera(vec3 lookAt, float followDist, float rot);
	~ThirdPersonCamera(){}														// blank inline destructor
	vec3 getEye(){return eye;}													// inline function that returns the ThirdPersonCamera Eye position as a vec3
	vec3 getAt(){return at;}												    // inline function that returns the ThirdPersonCamera At position as a vec3
	vec3 getUp(){return up;}													// inline function that returns the ThirdPersonCamera Up position as a vec3
	void updateEye(vec3 pos){eye = pos;}										// inline function that updates the Eye position via a passed in vec3 
	void updateAt(vec3 pos){at = pos;}											// inline function that updates the At position via a passed in vec3 
	void updateUp(vec3 pos){up = pos;}											// inline function that updates the Up position via a passed in vec3 
	void updateRotation(float rot){rotation = rot;}								// inline function that updates the ThirdPersonCamera rotation and renews the camera
	float getRotation(){return rotation;}										// inline function that returns the ThirdPersonCamera rotation as a float
	mat4 getCurrentCamera(){return lookAt(eye, at, up);}							// inline function that returns the ThirdPersonCamera LookAt as a mat4
	void updateFollowDistance(float followDist){followDistance = followDist;}   // inline function that updates the ThirdPersonCamera FollowDistance
	float getFollowDistance(){return followDistance;}							// inline function that returns the ThirdPersonCamera's FollowDistance
	void updateCamera(vec3 lookAt);												// function that needs to be called during update with the passed in lookAt position and this function will keep the cameras position behind the passed in point

private:
	// floats to hold the distance the eye travels behind the LookAt position
	float rotation;
	float followDistance;

	// vec3s to hold the eye, at and up
	vec3 eye;
	vec3 at;
	vec3 up;

};

#endif