#include "PlayerGameObject.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms
#include <sstream>					    // to use strstream to stream the score/hp into the updateHUDText function
#include <SDL_ttf.h>                    // including to use SDL_Colour to pass in to the PlayerHUD

PlayerGameObject::PlayerGameObject(vec3 pos, vec3 rot, vec3 scale, char * md2FileName, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions, int hp,  GLuint firstHUDImageTextureID, GLuint secondHUDImageTextureID)
{
	// setting up the position/scalar/rotation/hp/score/moveRotation
	position = pos;
	scalar = scale;
	rotation = rot;
	health = hp;
	score = 0;
	moveRotation = 0;

	// setting up a default red for the SDL colour, need to specify the B/G as the colour turns pink otherwise!
	textColour.r = 255;
	textColour.b = 0;
	textColour.g = 0;

	// instantiating the PlayerHUD using default values for positioning and colour, and passing in the HUD's images 
	characterHUD = new PlayerHUD(0.15, 0.25, 20, textColour, vec3(-1.5f, 1.5f, -4.0f), vec3(1.8f, 1.45f, -4.0f), vec3(-2.0f, 1.5f, -4.0f), vec3(1.3f, 1.5f, -4.0f), firstHUDImageTextureID, secondHUDImageTextureID);
	
	// updating the characterHUD text with the passed in hp and score 
	std::stringstream tempStrStream[2];
	tempStrStream[0] << "  Health: " << health;
	tempStrStream[1] << "  Score: " << score;
	characterHUD->updateHUDText(tempStrStream[0].str().c_str(), tempStrStream[1].str().c_str(), textColour);
	
	// instantiating the collisionBox with the passed in positions and dimensions
	collisionBox = new BoundingBox(collisionBoxPos, collisionBoxDimensions);

	// instantiating the MD2ModelHolder by passing in the md2Model 
	model = new MD2ModelHolder(md2FileName, textureID);
}

// function that updates the PlayerGameObject's hp with a passed in value
// and then updates the PlayerGameObject's PlayerHUD
void PlayerGameObject::updateHealth(int hp)
{
	health = hp;

	std::stringstream tempStrStream[2];
	tempStrStream[0] << "  Health: " << health;
	tempStrStream[1] << "  Score: " << score;
	characterHUD->updateHUDText(tempStrStream[0].str().c_str(), tempStrStream[1].str().c_str(), textColour);
}

// function that updates the PlayerGameObject's score with a passed in value
// and then updates the PlayerGameObject's PlayerHUD
void PlayerGameObject::updateScore(int nScore)
{
	score = nScore;

	std::stringstream tempStrStream[2];
	tempStrStream[0]<< " Health: " << health;
	tempStrStream[1] << "  Score: " << score;
	characterHUD->updateHUDText(tempStrStream[0].str().c_str(), tempStrStream[1].str().c_str(), textColour);
}

// taking in a shader/material/matrix and drawing the PlayerGameObject to screen using the specified position/rotation/scalar
void PlayerGameObject::draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix)
{
	model->animateAndUpdateModel();

	mvMatrix = translate(mvMatrix, vec3(position.x, position.y, position.z));
	mvMatrix = rotate(mvMatrix, rotation.x, vec3(1.0f, 0.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.y, vec3(0.0f, 1.0f, 0.0f));
	mvMatrix = rotate(mvMatrix, rotation.z, vec3(0.0f, 0.0f, 1.0f));
	mvMatrix = scale(mvMatrix, vec3(scalar.x, scalar.y, scalar.z));
	model->draw(shaderID, material, mvMatrix);
}

// function that takes in a shader and draws the characters hud
void PlayerGameObject::drawHUD(GLuint shaderID)
{
	characterHUD->draw(shaderID);
}

// function that moves the character forward or backwards by a passed in float
// and the distance is based off of the PlayerGameObject's current moveRotation
// also sets the PlayerGameObject's animation to run
void PlayerGameObject::moveCharacterForwardOrBackward(float dist)
{
	model->updateCurrentAnimation(1);
	position = rt3d::moveForward(position, moveRotation, dist);
}

// function that moves the character right or left by a passed in float
// and is the distance is based off of the PlayerGameObject's current moveRotation
// also sets the PlayerGameObject's animation to run
void PlayerGameObject::moveCharacterRightOrLeft(float dist)
{
	model->updateCurrentAnimation(1);
	position = rt3d::moveRight(position, moveRotation, dist);
}

// increments both the rotation and character rotation as these two values must be 
// kept seperate for the move functions to work correctly since axis rotation could start
// at 90 which would make the move character functions go in the wrong distance if used
// it also rotates the model around the Z axis instead of the Y axis as I have yet to get
// the md2 model to work around the normal axis rotations
void PlayerGameObject::rotateCharacter(float rot)
{
	moveRotation -= rot;
	rotation.z += rot;
}

// function that takes in a int value to update the md2 models current animation
void PlayerGameObject::updateCharacterAnimation(int anim)
{
	model->updateCurrentAnimation(anim);

	// checking that the animations aren't outside the specified amount of animations
	// if they are set them to the first animation or last animation
	if (model->getCurrentAnimation() < 0) 
	{
		model->updateCurrentAnimation(19);
	}
	else if (model->getCurrentAnimation() >= 20) 
	{
		model->updateCurrentAnimation(0); 
	}
}

// destructor that deletes all of objects instantiated within this class
PlayerGameObject::~PlayerGameObject()
{
	delete characterHUD;
	delete model;
	delete collisionBox;
}