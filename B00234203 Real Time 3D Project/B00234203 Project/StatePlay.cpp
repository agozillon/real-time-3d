#include "StatePlay.h"  // including the statePause header so I can code implementations of the functions
#include "GameControl.h"
#include <stack>        // including stack for access to the stack container

// Initialise function to set OpenGL values and game related values and variables
void StatePlay::init() 
{
	// assigning the number of each set of objects for loops and readabillity
	// and setting the maximumScore for this state
	numberOfWalls = 5;
	numberOfBarriers = 3;
	numberOfBuildings = 2;
	numberOfPickups = 3;
	numberOfSoundEffects = 2;
	numberOfEnemies = 3;
	maximumScore = 300;

	// setting the initial boolean values for state 
	dynamicLightMovementOn = true;
	playerMoving = false;

	// setting up the various variables required for temporary invincibility of the player
	// tempInvincibility is initilized as 3 so the player can actually get hit by the enemy
	// at the start 
	currentTime = clock();
	enemyCollisionTime = 0;
	tempInvincibility = 3;
	
	// initiating shaders
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	skyBoxProgram = rt3d::initShaders("skybox-shader.vert","skybox-shader.frag");

	// initiating and setting the DynamicLight, passed in values are ambient/diffuse/specular/position/scalar as well as the end traversal point on x and minimum and maximum heights
	light = new DynamicLight(0.3f, 0.3f, 0.3f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -10.0f, 10.0f, 20.0f, 1.0f, 170, 100, -25);
	light->setLight(shaderProgram);
	
	// instantiating the materials ambience/diffuse/specular/shine
	material = new Material(0.6, 0.6, 0.6, 1.0, 0.5, 0.5, 0.5, 1.0, 0.6, 0.6, 0.6, 1.0, 2.0);

	// instantiating and loading in the two sound effects I use 
	soundEffect[0] = new BassAudioLoader("button-2.wav");
	soundEffect[1] = new BassAudioLoader("button-3.wav");

	// instantiating the ModelAndTexutreHandler
	objModelAndTextureHandler = new ObjModelAndTextureHandler();

	// loading in the states .obj models
	objModelAndTextureHandler->loadAndStoreObjModel("cube.obj");					   // 0th obj model
	objModelAndTextureHandler->loadAndStoreObjModel("barrier.obj");				   // 1th obj model
	objModelAndTextureHandler->loadAndStoreObjModel("build11WithTriangulation.obj");  // 2th obj model
	objModelAndTextureHandler->loadAndStoreObjModel("retro_radio.obj");			   // 3th obj model
	objModelAndTextureHandler->loadAndStoreObjModel("Knight.obj");				       // 4th obj model

	// loading in the states textures
	objModelAndTextureHandler->loadAndStoreTexture("groundTexture1.bmp", true);	   // 0th texture
	objModelAndTextureHandler->loadAndStoreTexture("wallTexture2.bmp", false);		   // 1th texture
	objModelAndTextureHandler->loadAndStoreTexture("top.bmp", true);			       // 2th texture
	objModelAndTextureHandler->loadAndStoreTexture("left.bmp", true);				   // 3th texture
	objModelAndTextureHandler->loadAndStoreTexture("right.bmp", true);				   // 4th texture
	objModelAndTextureHandler->loadAndStoreTexture("back.bmp", true);				   // 5th texture
	objModelAndTextureHandler->loadAndStoreTexture("front.bmp", true);				   // 6th texture
	objModelAndTextureHandler->loadAndStoreTexture("hayden.bmp", false);			   // 7th texture
	objModelAndTextureHandler->loadAndStoreTexture("build11.bmp", false);	           // 8th texture
	objModelAndTextureHandler->loadAndStoreTexture("swarm_body_s.bmp", false);	       // 9th texture
	objModelAndTextureHandler->loadAndStoreTexture("Herz.bmp", false);				   // 10th texture
	objModelAndTextureHandler->loadAndStoreTexture("SelfMadeRadio.bmp", false);	   // 11th texture
	
	// setting up temporary GLuints to store and gather the Mesh/Texture/Index's that we are going to use
	// for each object
	GLuint tempTextureID[5];
	GLuint tempMeshID;
	GLuint tempIndexCount;

	// getting the texture ID's for the sky boxes and initiating the sky boxes, also setting the width/height for my SkyBox
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 2);
	objModelAndTextureHandler->getTextureID(tempTextureID[1], 3);
	objModelAndTextureHandler->getTextureID(tempTextureID[2], 4);
	objModelAndTextureHandler->getTextureID(tempTextureID[3], 5);
	objModelAndTextureHandler->getTextureID(tempTextureID[4], 6);
	skyBox = new SkyBox(tempTextureID[0], tempTextureID[1], tempTextureID[2], tempTextureID[3], tempTextureID[4]);
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated BasicGameObject's
	// and passing in the model position/scalar/rotation and collisionBox positions/dimensions
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 0);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 0);
	ground = new BasicGameObject(vec3(-10, -0.1, -10), vec3(0, 0, 0), vec3(120, 0.1, 120), tempMeshID, tempIndexCount, tempTextureID[0], vec3(-10, -0.1, -10), vec3(0, 0, 0));	
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated BasicGameObject's
	// and passing in the model position/scalar/rotation and collisionBox positions/dimensions
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 1);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 0);
	wall[0] = new BasicGameObject(vec3(-10, 0, -10), vec3(0, 0, 0), vec3(120, 10, 10), tempMeshID, tempIndexCount, tempTextureID[0], vec3(50, 5, -5), vec3(60, 5, 5));
	wall[1] = new BasicGameObject(vec3(-10, 0, 100), vec3(0, 0, 0), vec3(120, 10, 10), tempMeshID, tempIndexCount, tempTextureID[0], vec3(50, 5, 105), vec3(60, 5, 5));
	wall[2] = new BasicGameObject(vec3(-10, 0, 0), vec3(0, 0, 0), vec3(10, 10, 100), tempMeshID, tempIndexCount, tempTextureID[0], vec3(-5, 5, 50), vec3(5, 5, 50));
	wall[3] = new BasicGameObject(vec3(100, 0, 0), vec3(0, 0, 0), vec3(10, 10, 50), tempMeshID, tempIndexCount, tempTextureID[0], vec3(105, 5, 25), vec3(5, 5, 25));
	wall[4] = new BasicGameObject(vec3(100, 0, 60), vec3(0, 0, 0), vec3(10, 10, 40), tempMeshID, tempIndexCount, tempTextureID[0], vec3(105, 5, 80), vec3(5, 5, 20));
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated BasicGameObject's
	// and passing in the model position/scalar/rotation and collisionBox positions/dimensions
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 1);
	barrier[0] = new BasicGameObject(vec3(100, 0, 52), vec3(0, 90, 0), vec3(0.25, 0.25, 0.27), tempMeshID, tempIndexCount, tempTextureID[0], vec3(100, 2, 51.6), vec3(0.5, 2, 1.7));
	barrier[1] = new BasicGameObject(vec3(100, 0, 55), vec3(0, 90, 0), vec3(0.25, 0.25, 0.27), tempMeshID, tempIndexCount, tempTextureID[0], vec3(100, 2, 55), vec3(0.5, 2, 1.7));
	barrier[2] = new BasicGameObject(vec3(100, 0, 58), vec3(0, 90, 0), vec3(0.25, 0.25, 0.27), tempMeshID, tempIndexCount, tempTextureID[0], vec3(100, 2, 58.4), vec3(0.5, 2, 1.7));
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated BasicGameObject's
	// and passing in the model position/scalar/rotation and collisionBox positions/dimensions
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 8);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 2);
	building[0] = new BasicGameObject(vec3(15, 0, 70), vec3(0, 90, 0), vec3(0.25, 0.25, 0.25), tempMeshID, tempIndexCount, tempTextureID[0], vec3(15, 28, 70), vec3(15, 28, 17.5)); 
	building[1] = new BasicGameObject(vec3(50, 0, 15), vec3(0, 0, 0), vec3(0.25, 0.25, 0.25), tempMeshID, tempIndexCount, tempTextureID[0],  vec3(50, 28, 15), vec3(17.5, 28, 15)); 
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated ScoreItemGameObject's
	// and passing in the model position/scalar/rotation and collisionBox positions/dimensions 
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 2);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 3);
	pickups[0] = new ScoreItemGameObject(vec3(15, 1.5, 20), vec3(0, 180, 0), vec3(0.3, 0.3, 0.3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(15, 1.5, 20), vec3(0.8, 1, 0.8));
	pickups[1] = new ScoreItemGameObject(vec3(90, 1.5, 20), vec3(0, 180, 0), vec3(0.3, 0.3, 0.3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(90, 1.5, 20), vec3(0.8, 1, 0.8));
	pickups[2] = new ScoreItemGameObject(vec3(15, 1.5, 90), vec3(0, 270, 0), vec3(0.3, 0.3, 0.3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(15, 1.5, 90), vec3(1, 1, 1));
	
	// getting the texture ID/Mesh ID/ Index count and passing them into the newly instantiated EnemyGameObject's 
	// and also passing in the model position/scalar/rotation and collisionBox/agroBox positions/dimensions
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 9);
	objModelAndTextureHandler->getObjModel(tempMeshID, tempIndexCount, 4);
	enemy[0] = new EnemyGameObject(vec3(40, 0, 90), vec3(0, 180, 0), vec3(3, 3, 3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(40, 0, 90), vec3(0.5, 2.0, 0.5), vec3(40.0, 0, 90.0), vec3(10.0, 10.0, 10.0));
	enemy[1] = new EnemyGameObject(vec3(20, 0, 20), vec3(0, 180, 0), vec3(3, 3, 3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(20, 0, 20), vec3(0.5, 2.0, 0.5), vec3(20.0, 0, 20.0), vec3(10.0, 10.0, 10.0));
	enemy[2] = new EnemyGameObject(vec3(90, 0, 30), vec3(0, 180, 0), vec3(3, 3, 3), tempMeshID, tempIndexCount, tempTextureID[0], vec3(90, 0, 30), vec3(0.5, 2.0, 0.5), vec3(90.0, 0, 30.0), vec3(10.0, 10.0, 10.0));

	// getting 3 texture IDs for the PlayerGameObject (Md2Model and 2 HUD images) also don't have a md2Model handler extension for the handler class so 
	// passing in the md2 file name and also passing in the hp points, model position/scalar/rotation and collisionBox positions/dimensions 
	objModelAndTextureHandler->getTextureID(tempTextureID[0], 7);
	objModelAndTextureHandler->getTextureID(tempTextureID[1], 10);
	objModelAndTextureHandler->getTextureID(tempTextureID[2], 11);
	playerCharacter = new PlayerGameObject(vec3(90.0, 1.2, 60.0), vec3(-90, 0, 90), vec3(0.05, 0.05, 0.05), "hayden-tris.md2", tempTextureID[0], vec3(90.0, 1.2, 60.0), vec3(0.8, 1.1, 0.8), 3, tempTextureID[1], tempTextureID[2]);

	// instantiating the ThirdPersonCamera looking at the player and set with a follow distance of -6.0 and a rotation of 0
	camera = new ThirdPersonCamera(playerCharacter->getPosition(), -6.0, 0);
	
	// enabling depth test/blend and cull face and specifcying the settings for 
	// the depth test and blend
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LESS);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


// draw function for the StatePlay class
void StatePlay::draw()
{
	// clear the screen and buffer
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	// get the initial projection matrix (far is quite high for my own SkyBox)
	mat4 projection(1.0);
	projection = perspective(60.0f, 800.0f/600.0f, 1.0f, 200.0f);
	
	// passing in identity matrix to mvStack
	std::stack<mat4> mvStack; 
	mvStack.push(mat4(1.0));

	// setting the shader program to the skybox shader and then setting the projection
	// with the skybox shader, then setting the stack.top to the currentCamera positions
	// then creating a mat3 to get the cameras rotation seperated then putting it back into 
	// a mat4 and passing it into the LabSkyBox to draw it and then popping the stack
	glUseProgram(skyBoxProgram);
	rt3d::setUniformMatrix4fv(skyBoxProgram, "projection", value_ptr(projection));
	mvStack.top() = camera->getCurrentCamera();
	mat3 mvRotOnlyMat3 = mat3(mvStack.top());
	mvStack.push(mat4(mvRotOnlyMat3));
	skyBox->draw(skyBoxProgram, mvStack.top());
	mvStack.pop();

	// setting it back to the normal phong shader 
	glUseProgram(shaderProgram);

	// resetting projection matrix using the shaderProgram
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", value_ptr(projection));

	// moves and updates the lights position in accordance to the camera position and then setting it
	vec4 temp = mvStack.top()*light->getLightVectorPositionAndScalar();
	light->updateLightStructPositionAndScalar(temp.x, temp.y, temp.z, light->getLightStructPositionAndScalar().w);
	light->setLight(shaderProgram); 

	// draw the ground using the mvStack.top,  material and shaderProgram
	ground->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	
	// loop through and draw the walls using the mvStack.top,  material and shaderProgram
	for(int i = 0; i < numberOfWalls; i++)
	{
		wall[i]->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	}

	// loop through and draw the barrier's using the mvStack.top,  material and shaderProgram
	for(int i = 0; i < numberOfBarriers; i++)
	{
		barrier[i]->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	}

	// loop through and draw the building's using the mvStack.top,  material and shaderProgram
	for(int i = 0; i < numberOfBuildings; i++)
	{
		building[i]->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	}

	// loop through and draw the pickups's using the mvStack.top,  material and shaderProgram
	for(int i = 0; i < numberOfPickups; i++)
	{
		pickups[i]->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	}

	// loop through and draw the enemy's using the mvStack.top,  material and shaderProgram
	for (int i = 0; i < numberOfEnemies; i++)
	{
		enemy[i]->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top());
	}

	//draw the playerCharacter using the mvStack.top,  material and shaderProgram
	playerCharacter->draw(shaderProgram, material->getFullMaterialStruct(), mvStack.top()); 

	// sets the shader program to the skyBoxProgram, passes it into the playerCharacter drawHUD function
	// and then sets the shader back to the shaderProgram
	glUseProgram(skyBoxProgram);
	playerCharacter->drawHUD(skyBoxProgram);
	glUseProgram(shaderProgram);
	mvStack.pop(); // popping the stack to 0 
	
	SDL_GL_SwapWindow(GameControl::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StatePlay::update()
{
	// unsinged 8 bit int pointer that gets set to the current keyboard state
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	
	// checking if the playerMoving bool is set to true 
	// every update so that if the player has been moving
	// and stopped recently the run animation stops and goes 
	// to idle holding. Not ideal just a quick fix till i get
	// time over summer to integrate animations
	if(playerMoving == true)
	{
		playerCharacter->updateCharacterAnimation(0);
	}

	// if the dynamicLightMovementOn is true then move the light using DynamicLight
	if (dynamicLightMovementOn == true)
	{
		light->moveDynamicLight();
	}

	// sets currentTime to the clocks function time in seconds
	currentTime = clock()/CLOCKS_PER_SEC;

	// updates the player collision box to the playerCharacters current position
	playerCharacter->collisionBox->updatePosition(playerCharacter->getPosition());

	// loops through the array of Items and updates there rotation axis so that they spin on the y axis and
	// then checks if the players collision box has intersected an items collision box if it has check if its visible
	// bool is set to true if it is true then set the visible bool to false, update the players score by the item score increase
	// and play the second sound effect
	for(int i = 0; i < numberOfPickups; i++)
	{
		pickups[i]->updateRotation(vec3(pickups[i]->getRotation().x, pickups[i]->getRotation().y+0.1, pickups[i]->getRotation().z));

		if (playerCharacter->collisionBox->detectCollision(pickups[i]->collisionBox) && pickups[i]->getIsVisible() == true)
		{
			pickups[i]->updateIsVisible(false);
			playerCharacter->updateScore(playerCharacter->getScore()+pickups[i]->getScoreIncrease());
			soundEffect[1]->playAudio();	
		}
	}

	// loops through the Walls and checks if the players collision box has collided with any of the walls collision boxs
	// if the player has. Then update the players position by the walls collider(player) position before collision
	// else continue to update the barriers collider position before collision with the player characters position
	for(int i = 0; i < numberOfWalls; i++)
	{
		if (playerCharacter->collisionBox->detectCollision(wall[i]->collisionBox))
		{
			playerCharacter->updatePosition(wall[i]->collisionBox->getColliderPosition());
		}
		else
		{
			wall[i]->collisionBox->updateColliderPosition(playerCharacter->getPosition());
		}	
	}

	// loops through the barriers and checks if the players collision box has collided with any of the barriers collision boxs
	// if the player has. Then update the players position by the barriers collider(player) position before collision
	// else continue to update the barriers collider position before collision with the player characters position
	for(int i = 0; i < numberOfBarriers; i++)
	{
		if (playerCharacter->collisionBox->detectCollision(barrier[i]->collisionBox))
		{
			playerCharacter->updatePosition(barrier[i]->collisionBox->getColliderPosition());
		}
		else
		{
			barrier[i]->collisionBox->updateColliderPosition(playerCharacter->getPosition());
		}
	}

	// loops through the buildings and checks if the players collision box has collided with any of the buildings collision boxs
	// if the player has. Then update the players position by the buildings collider(player) position before collision
	// else continue to update the buildings collider position before collision with the player characters position
	for(int i = 0; i < numberOfBuildings; i++)
	{
		if (playerCharacter->collisionBox->detectCollision(building[i]->collisionBox))
		{
			playerCharacter->updatePosition(building[i]->collisionBox->getColliderPosition());
		}
		else
		{
			building[i]->collisionBox->updateColliderPosition(playerCharacter->getPosition());
		}	
	}

	// loops through the enemies and checks if there agro box collides with the players collision box
	// if it does then start playing the first  audio file and then start movin the enemy towards the player 
	// character via the moveToOtherGame object function and also start updating the enemys collision/agro boxs to
	// the enemys position. Also start checking for the enemys collision box intersecting the player character collision box
	// if it does and the temporaryInvincibility timer is over 1(Second) then take away the players hp and set the enemyCollisionTime
	// to the clock time in seconds 
	for (int i = 0; i < numberOfEnemies; i++)
	{
		if(playerCharacter->collisionBox->detectCollision(enemy[i]->agroBox))
		{
			soundEffect[0]->playAudio();

			enemy[i]->moveToOtherGameObject(playerCharacter, 0.05, 0.05);
			enemy[i]->collisionBox->updatePosition(enemy[i]->getPosition());
			enemy[i]->agroBox->updatePosition(enemy[i]->getPosition());
			
			if(enemy[i]->collisionBox->detectCollision(playerCharacter->collisionBox) && tempInvincibility > 1)
			{
				playerCharacter->updateHealth(playerCharacter->getHealth()-1);
				enemyCollisionTime = clock()/CLOCKS_PER_SEC;
			}
		}
	}

	// takes away the enemyCollisionTime from the currentTime to get the temporaryInvincibility timer
	// should reset to 0 every time the player intersects an enemy (didn't have time to encapsulate)
	tempInvincibility = currentTime - enemyCollisionTime;


	// if the playerCharacters lives are less than or equal 0 then set state to
	// the GameOverState
	if (playerCharacter->getHealth() <= 0)
	{
		GameControl::getInstance()->setState(GameControl::getInstance()->getGameOverState());
	}

	// if the playerCharacters score is equal to the maximumScore then set state to
	// the GameWonState
	if (playerCharacter->getScore() == maximumScore)
	{
		GameControl::getInstance()->setState(GameControl::getInstance()->getGameWonState());
	}

	// move playerCharacter forward when W is pressed
	if (keys[SDL_SCANCODE_W])
	{
		playerMoving = true;
		playerCharacter->moveCharacterForwardOrBackward(0.1);
	}

	// move playerCharacter back when S is pressed
	if (keys[SDL_SCANCODE_S])
	{
		playerMoving = true;
		playerCharacter->moveCharacterForwardOrBackward(-0.1);
	}

	// move playerCharacter left when A is pressed
	if (keys[SDL_SCANCODE_A])
	{
		playerMoving = true;
		playerCharacter->moveCharacterRightOrLeft(-0.1);
	}

	// move playerCharacter right when S is pressed
	if (keys[SDL_SCANCODE_D])
	{
		playerMoving = true;
		playerCharacter->moveCharacterRightOrLeft(0.1);
	}
	
	// rotates the camera to the left and the character to the right when , is pressed
	if (keys[SDL_SCANCODE_COMMA]) 
	{
		playerCharacter->rotateCharacter(1.0);
		camera->updateRotation(camera->getRotation() - 1.0);
	}

	// rotates the camera to the right and the character to the left when . is pressed 
	if (keys[SDL_SCANCODE_PERIOD])  
	{
		playerCharacter->rotateCharacter(-1.0);
		camera->updateRotation(camera->getRotation() + 1.0);
	}

	// turns the DynamicLights movement on and off 
	// (since the calls in update it switches on and off incredibly fast)
	if (keys[SDL_SCANCODE_L])
	{
		if(dynamicLightMovementOn == true)
		{
			dynamicLightMovementOn = false;
		}
		else
		{
			dynamicLightMovementOn = true;
		}
	}

	// press Z key to decrement through the playerCharacters md2 models animations
	if (keys[SDL_SCANCODE_Z])
	{
		playerCharacter->updateCharacterAnimation(playerCharacter->getCharacterCurrentAnimation()-1);
		std::cout << "Current animation: " << playerCharacter->getCharacterCurrentAnimation() << std::endl;
	}

	// press X key to decrement through the playerCharacters md2 models animations
	if (keys[SDL_SCANCODE_X])
	{
		playerCharacter->updateCharacterAnimation(playerCharacter->getCharacterCurrentAnimation()+1);
		std::cout << "Current animation: " << playerCharacter->getCharacterCurrentAnimation() << std::endl;
	}

	// call to update the cameras position to lookAt playerCharacters position
	camera->updateCamera(playerCharacter->getPosition());
}

// used to perform actions upon entry to this game state
void StatePlay::enter()
{
	//resetting the essentials within this function

	// reseting character variables to original settings, don't need to reset the bounding box as the update sets it to the 
	// model position, Ideally each object would have its own reset function...
	playerCharacter->updateHealth(3);
	playerCharacter->updateScore(0);
	playerCharacter->updatePosition(vec3(90.0, 1.2, 60.0));
	playerCharacter->updateRotation(vec3(-90, 0, 90));
	playerCharacter->updateMoveRotation(0);
	playerCharacter->updateCharacterAnimation(0);

	// reseting camera rotation no need to reset the position as the update function positions the 
	// camera on the player
	camera->updateRotation(0);

	// resetting the items visibility so that they are again visible and the player can pick them up again for
	// points 
	for(int i = 0; i < numberOfPickups; i++)
	{
		pickups[i]->updateIsVisible(true);
	}
	
	// resetting enemy position and rotation to there original points
	enemy[0]->updatePosition(vec3(40, 0, 90));
	enemy[0]->updateRotation(vec3(0, 180, 0));
	enemy[1]->updatePosition(vec3(20, 0, 20));
	enemy[1]->updateRotation(vec3(0, 180, 0));
	enemy[2]->updatePosition(vec3(90, 0, 30));
	enemy[2]->updateRotation(vec3(0, 180, 0));
	
	// resetting the bounding boxes of the enemies to there reset positions
	for (int i = 0; i < numberOfEnemies; i++)
	{
		enemy[i]->collisionBox->updatePosition(enemy[i]->getPosition());
		enemy[i]->agroBox->updatePosition(enemy[i]->getPosition());
	}
}

// destructor that deletes all objects instantiated within this class
StatePlay::~StatePlay()
{
	delete light;
	delete material;
	delete playerCharacter;
	delete camera;
	delete ground;
	delete objModelAndTextureHandler;
	delete skyBox;

	for (int i = 0; i < numberOfEnemies; i++)
	{
		delete enemy[i];
	}

	for (int i = 0; i < numberOfSoundEffects; i++)
	{
		delete soundEffect[i];
	}

	for(int i = 0; i < numberOfPickups; i++)
	{
		delete pickups[i];
	}

	for(int i = 0; i < numberOfBuildings; i++)
	{
		delete building[i];
	}

	for(int i = 0; i < numberOfWalls; i++)
	{
		delete wall[i];
	}

	for(int i = 0; i < numberOfBarriers; i++)
	{
		delete barrier[i];
	}
}
