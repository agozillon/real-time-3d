#include "BoundingBox.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms

// basic constructor that accepts the BoundingBox's position and dimensions and sets them up
BoundingBox::BoundingBox(vec3 pos, vec3 bDimensions)
{
	dimensions.x = bDimensions.x;
	dimensions.y = bDimensions.y;
	dimensions.z = bDimensions.z;

	position.x = pos.x;
	position.y = pos.y;
	position.z = pos.z;
}

// function that accepts a pointer to a BoundingBox and checks for a collision between the current bounding box
// and the passed in BoundingBox and then returns either true or false depending on if they've colided
bool BoundingBox::detectCollision(BoundingBox * colideeBoundingBox)
{
	// sets the initial collision return bool to false
	bool didCollisionTakePlace = false;

	// really simple Axis alligned bounding box detection checks if one side of a bounding box is in another
	// and then returns a true value if it is indented this way for ease of reading
	if ((position.x + dimensions.x > colideeBoundingBox->getPosition().x - colideeBoundingBox->getDimensions().x) 
	&& (position.x - dimensions.x < colideeBoundingBox->getPosition().x + colideeBoundingBox->getDimensions().x)
	&& (position.y + dimensions.y > colideeBoundingBox->getPosition().y - colideeBoundingBox->getDimensions().y)
	&& (position.y - dimensions.y < colideeBoundingBox->getPosition().y + colideeBoundingBox->getDimensions().y)					
	&& (position.z + dimensions.z > colideeBoundingBox->getPosition().z - colideeBoundingBox->getDimensions().z)						
	&& (position.z - dimensions.z < colideeBoundingBox->getPosition().z + colideeBoundingBox->getDimensions().z))
	{
		didCollisionTakePlace = true;	
	}

	// returns the bool 
	return didCollisionTakePlace;
}

// function that creates a cube to visually represent the bounding box based off of the bounding box's dimensions
void BoundingBox::createVisibleBoundingBox()
{
	// the cubes index count and vertex count
	cubeIndexCount = 36;
	vertexCount = 8;

	// hard coded indices for the cube
	GLuint cubeIndices[] = 
	{
		0,1,2, 0,2,3, // back face 
		1,0,5, 0,4,5, // left face
		6,3,2, 3,6,7, // right face
		1,5,6, 1,6,2, // top face
		0,3,4, 3,7,4, // bottom face
		6,5,4, 7,6,4  // front face
	}; 

	// list of cube vertices being setup alligned like this for ease of reading and understanding 
	// uses the dimensions of the bounding box to create it
    cubeVertices[0] = -dimensions.x;  cubeVertices[1] = -dimensions.y;  cubeVertices[2] = -dimensions.z;  // vert 0
	cubeVertices[3] = -dimensions.x;  cubeVertices[4] = dimensions.y;   cubeVertices[5] = -dimensions.z;  // vert 1
	cubeVertices[6] = dimensions.x;   cubeVertices[7] = dimensions.y;   cubeVertices[8] = -dimensions.z;  // vert 2
	cubeVertices[9] = dimensions.x;   cubeVertices[10] = -dimensions.y; cubeVertices[11] = -dimensions.z; // vert 3
	cubeVertices[12] = -dimensions.x; cubeVertices[13] = -dimensions.y; cubeVertices[14] = dimensions.z;  // vert 4
	cubeVertices[15] = -dimensions.x; cubeVertices[16] = dimensions.y;  cubeVertices[17] = dimensions.z;  // vert 5
	cubeVertices[18] = dimensions.x;  cubeVertices[19] = dimensions.y;  cubeVertices[20] = dimensions.z;  // vert 6
	cubeVertices[21] = dimensions.x;  cubeVertices[22] = -dimensions.y; cubeVertices[23] = dimensions.z;  // vert 7

	// calling rt3d's createMesh to create the object
	meshObject = rt3d::createMesh(vertexCount, cubeVertices, nullptr, cubeVertices, nullptr, cubeIndexCount, cubeIndices);
}

// function that simply draws the bounding box at its specificed positions and takes in a shaderID and matrix to do it
void BoundingBox::drawVisibleBoundingBox(GLuint shaderID, mat4 mvMatrix)
{	
	mvMatrix = translate(mvMatrix, position);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(mvMatrix));
	rt3d::drawIndexedMesh(meshObject, cubeIndexCount, GL_TRIANGLES);
}
