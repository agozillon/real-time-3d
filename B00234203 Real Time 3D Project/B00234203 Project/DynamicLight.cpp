#include "DynamicLight.h"

// constructor taking in values for the light struct and values for the traversal distance on the x and maximum height 
// then setting them up using the private functions
DynamicLight::DynamicLight(float ambR, float ambG, float ambB, float ambA, float diffR, float diffG, float diffB, float diffA, float specR, float specG, float specB, float specA, float posX, float posY, float posZ, float scale, float endTravelX, float maxTravelH, float minTravelH)
{
	// boolean which reverses the light if its true
	lightReverseDirection = false;

	// setting up the start/end travelling points from the passed in posX and the endTravelX
	startTravelPointX = posX;
	endTravelPointX = endTravelX;
	
	// setting the maximum and minimum travelling heights for the dynamic light via passed in values
	maxTravelHeight = maxTravelH;
	minTravelHeight = minTravelH;
	
	// calculating the mid travelling point on the X by passing in the start travelling point
	// and the end travelling point
	midTravelPointX = calculateMidPoint(startTravelPointX, endTravelPointX);
	
	// calculating the start point to mid point slope value by passing in the startTravelPointX/minTravelHeight and the midTravelPointX/maxTravelHeight
	// to the calculate slope function
	startMidSlopeValue = calculateSlope(startTravelPointX, minTravelHeight, midTravelPointX, maxTravelHeight);

	// calculating the mid to end slope value by passing in the midTravelPointX/maxTravelHeight and endTravelPointX/minTravelHeight 
	// to the calculate slope function
	midEndSlopeValue = calculateSlope(midTravelPointX, maxTravelHeight, endTravelPointX, minTravelHeight);
	
	// if the startMidSlopeValue or midEndSlopeValue are negative then set them to a positive value by negating them
	// as if we do not then the increment values will not work as they should in the update light function and cause it to move in the wrong direction
	if(startMidSlopeValue < 0)
	{
		startMidSlopeValue = -startMidSlopeValue;
	}

	if(midEndSlopeValue < 0)
	{
		midEndSlopeValue = -midEndSlopeValue;
	}

	// setting all the various light values using function calls to condense it 
	updateAmbience(ambR, ambG, ambB, ambA);
	updateDiffuse(diffR, diffG, diffB, diffA);
	updateSpecular(specR, specG, specB, specA);
	updateLightStructPositionAndScalar(posX, posY, posZ, scale);
	updateLightVectorPositionAndScalar(posX, posY, posZ, scale);
}

// function that updates the DynamicLights light structures ambience values
void DynamicLight::updateAmbience(float r, float g, float b, float a)
{
	light.ambient[0] = r;
	light.ambient[1] = g;
	light.ambient[2] = b;
	light.ambient[3] = a;
}

// function that updates the DynamicLights light structures diffuse values 
void DynamicLight::updateDiffuse(float r, float g, float b, float a)
{
	light.diffuse[0] = r;
	light.diffuse[1] = g;
	light.diffuse[2] = b;
	light.diffuse[3] = a;
}

// function that updates the DynamicLights light structures specular values
void DynamicLight::updateSpecular(float r, float g, float b, float a)
{
	light.specular[0] = r;
	light.specular[1] = g;
	light.specular[2] = b;
	light.specular[3] = a;
}

// function that updates the DynamicLights light structures position and scalar values
void DynamicLight::updateLightStructPositionAndScalar(float posX, float posY, float posZ, float scale)
{
	light.position[0] = posX;
	light.position[1] = posY;
	light.position[2] = posZ;
	light.position[3] = scale;
}

// function that updates the DynamicLights lightPositionAndScalar position and scalar values
void DynamicLight::updateLightVectorPositionAndScalar(float posX, float posY, float posZ, float scale)
{
	lightPositionAndScalar.x = posX;
	lightPositionAndScalar.y = posY;
	lightPositionAndScalar.z = posZ;
	lightPositionAndScalar.w = scale;
}

// function that returns the DynamicLight Light structures ambience as a vec4
vec4 DynamicLight::getLightStructAmbience()
{
	return vec4(light.ambient[0], light.ambient[1], light.ambient[2], light.ambient[3]);
}

// function that returns the DynamicLight Light structures diffuse as a vec4
vec4 DynamicLight::getLightStructDiffuse()
{
	return vec4(light.diffuse[0], light.diffuse[1], light.diffuse[2], light.diffuse[3]);
}

// function that returns the DynamicLight Light structures specular as a vec4
vec4 DynamicLight::getLightStructSpecular()
{
	return vec4(light.specular[0], light.specular[1], light.specular[2], light.specular[3]);
}

// function that returns the DynamicLight light structures position as a vec4
vec4 DynamicLight::getLightStructPositionAndScalar()
{
	return vec4(light.position[0], light.position[1], light.position[2], light.position[3]);
}

// updating the startTravelPointX by a passed in value and then recalculating
// the midTravelPointX and relevant slope values to keep the light moving
// correctly
void DynamicLight::updateStartTravelPointX(float posX)
{
	startTravelPointX = posX;
	
	// recalculating the midTravelPointX 
	midTravelPointX = calculateMidPoint(startTravelPointX, endTravelPointX);
	
	// recalculating the startMidSlopeValue
	startMidSlopeValue = calculateSlope(startTravelPointX, minTravelHeight, midTravelPointX, maxTravelHeight);
	
	// negating the slope if its a -ve to make it positive
	if(startMidSlopeValue < 0)
	{
		startMidSlopeValue = -startMidSlopeValue;
	}
}

// updating the endTravelPointX by a passed in value and then recalculating
// the midTravelPointX and relevant slope values to keep the light moving
// correctly
void DynamicLight::updateEndTravelPointX(float posX)
{
	endTravelPointX = posX;

	// recalculating the midTravelPointX 
	midTravelPointX = calculateMidPoint(startTravelPointX, endTravelPointX);
	
	// recalculating the midEndSlopeValue
	midEndSlopeValue = calculateSlope(midTravelPointX, maxTravelHeight, endTravelPointX, minTravelHeight);
	
	// negating the slope if its a -ve to make it positive
	if(midEndSlopeValue < 0)
	{
		midEndSlopeValue = -midEndSlopeValue;
	}
}

// updating the maximumTravellingOnY by a passed in value and then recalculating
// both of the slope values to keep the light moving correctly
void DynamicLight::updateMaxTravelHeight(float posY)
{
	maxTravelHeight = posY;

	// recalculating both of the slopes 
	startMidSlopeValue = calculateSlope(startTravelPointX, minTravelHeight, midTravelPointX, maxTravelHeight);
	midEndSlopeValue = calculateSlope(midTravelPointX, maxTravelHeight, endTravelPointX, minTravelHeight);

	// negating the slopes if they are -ve to make them positive
	if(midEndSlopeValue < 0)
	{
		midEndSlopeValue = -midEndSlopeValue;
	}

	if(startMidSlopeValue < 0)
	{
		startMidSlopeValue = -startMidSlopeValue;
	}
}

// updating the minTravelHeight by a passed in value and then recalculating
// both of the slope values to keep the light moving correctly
void DynamicLight::updateMinTravelHeight(float posY)
{
	minTravelHeight = posY;

	// recalculating both of the slopes 
	startMidSlopeValue = calculateSlope(startTravelPointX, minTravelHeight, midTravelPointX, maxTravelHeight);
	midEndSlopeValue = calculateSlope(midTravelPointX, maxTravelHeight, endTravelPointX, minTravelHeight);
	
	// negating the slopes if they are -ve to make them positive
	if (midEndSlopeValue < 0)
	{
		midEndSlopeValue = -midEndSlopeValue;
	}

	if (startMidSlopeValue < 0)
	{
		startMidSlopeValue = -startMidSlopeValue;
	}
}

// function that moves the dynamic light betweem the start point up to the mid point and then to the
// end point then reverses it, it should be called continuously within an update loop
void DynamicLight::moveDynamicLight()
{	
	// moves the light through its initial direction without it being reversed
	// if the lights position x is less than the middle travelling point and the lightReverseDirection
	// bool is false then move the light at an upwards angle towards the mid point else if the
	// lights position x is greater than the mid travelling point (passed the top of the arc) and reverse direction
	// is still false then move the light position in a downwards angle towards the end positon
	if(lightPositionAndScalar.x < midTravelPointX && lightReverseDirection == false)
	{
		updateLightVectorPositionAndScalar(lightPositionAndScalar.x+startMidSlopeValue, lightPositionAndScalar.y+startMidSlopeValue, lightPositionAndScalar.z, lightPositionAndScalar.w);
	}
	else if(lightPositionAndScalar.x >= midTravelPointX && lightReverseDirection == false)
	{
		updateLightVectorPositionAndScalar(lightPositionAndScalar.x+midEndSlopeValue, lightPositionAndScalar.y-midEndSlopeValue, lightPositionAndScalar.z, lightPositionAndScalar.w);
	}

	// if the light position x has passed the end travelling point on the x then reverse the 
	// direction of the light otherwise the lights travelling direction is not reversed
	if(lightPositionAndScalar.x > endTravelPointX)
	{
		lightReverseDirection = true;
	}
	else if(lightPositionAndScalar.x < startTravelPointX)
	{
		lightReverseDirection = false;
	}

	// moves the light in a reversed direction
	// checks if the lights position x is less than the mid travelling point and if the 
	// light reverse direction bool is true and if it is then it moves the light in a downward angle
	// from the mid point other wise it checks if its greater than the mid travelling point and if the reversed direction is
	// true and if it is it moves the light at an upwards angle towards the mid travelling point
	if(lightPositionAndScalar.x < midTravelPointX && lightReverseDirection == true)
	{
		updateLightVectorPositionAndScalar(lightPositionAndScalar.x-startMidSlopeValue, lightPositionAndScalar.y-startMidSlopeValue, lightPositionAndScalar.z, lightPositionAndScalar.w);
	}
	else if(lightPositionAndScalar.x >= midTravelPointX && lightReverseDirection == true)
	{
		updateLightVectorPositionAndScalar(lightPositionAndScalar.x-midEndSlopeValue, lightPositionAndScalar.y+midEndSlopeValue, lightPositionAndScalar.z, lightPositionAndScalar.w);
	}
}

// calculates the mid point from two passed in points using standard vector maths
float DynamicLight::calculateMidPoint(float firstPoint, float secondPoint)
{
	return (firstPoint + secondPoint) / 2;
}

// basic function that calculates a slope from 2 passed in 2D positions
float DynamicLight::calculateSlope(float firstPosX, float firstPosY, float secondPosX, float secondPosY)
{
	float tempX;
	float tempY;

	tempX = firstPosX - secondPosX;
	tempY = firstPosY - secondPosY;

	return tempX/tempY;
}
