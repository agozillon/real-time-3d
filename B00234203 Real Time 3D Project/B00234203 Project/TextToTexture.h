#ifndef	TEXTTOTEXTURE_H
#define TEXTTOTEXTURE_H
#include <SDL_ttf.h> // including SDL true type font library to use ttf functions and types
#include <GL\glew.h> // including glew to use GLuints

class TextToTexture 
{

public:
	TextToTexture(TTF_Font *font);								    // constructor that takes in a true type font and sets it up
	~TextToTexture();												// destructor used to close the ttf_font in this case										
	GLuint createTextTexture(const char * str, SDL_Color colour);   // function that creates a ttf_font texture from a passed in set of chars and 3 ints representing RGB and then returns it as a textureID

private:
	// text font pointer to hold the passed in font
	TTF_Font* textFont;  

};

#endif
