#ifndef	PLAYERGAMEOBJECT_H
#define PLAYERGAMEOBJECT_H
#include "MD2ModelHolder.h"		// including the MD2ModelHolder so that I can have an MD2ModelHolder object
#include "PlayerHUD.h"			// PlayerHUD so that I can have a PlayerHUD object, also gives access to SDL_ttf related stuff
#include "AbstractGameObject.h" // AbstractGameObject so PlayerGameObject can inherit from the AbstractGameObject and use its includes


// this is the PlayerGameObject class it inherits from AbstractGameObject and incorporates an md2Model and characterHUD
// its used to draw a character and UI to screen, move the model and keep score/hp values
class PlayerGameObject: public AbstractGameObject
{

public:
	// constructor that takes in a set of chars for the model name, a texture ID, the PlayerGameObjects health points, its position/rotation/scale and 2 texutreIDs for the HUD images and positions/dimensions for its bounding box
	PlayerGameObject(vec3 pos, vec3 rot, vec3 scale, char * md2FileName, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions, int hp,  GLuint firstHUDImageTextureID, GLuint secondHUDImageTextureID);
	~PlayerGameObject();																// destructor used to delete all instantiated objects
	vec3 getPosition(){return position;}												// inline function that returns the position as a vec3
	void updatePosition(vec3 pos){position = pos;}										// inline function that takes a vec3 and updates the position
	vec3 getRotation(){return rotation;}												// inline function that returns the rotation as a vec3			 
	void updateRotation(vec3 rot){rotation = rot;}							    		// inline function that takes a vec3 and updates the rotation
	vec3 getScalar(){return scalar;}													// inline function that returns the scalar as a vec3
	void updateScalar(vec3 scale){scalar = scale;}										// inline function that takes a vec3 and updates the scalar
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);			// function that takes a shader/material/matrix and uses them to draw the model
	int getScore(){return score;}														// function that returns the PlayerGameObjects score value
	int getHealth(){return health;}														// function that returns the PlayerGameObjects hp value
	void updateHealth(int hp);															// function used to update the PlayerGameObjects hp not inline because it also updates the HUD Text texture
	void updateScore(int nScore);														// function used to update the PlayerGameObjects score not inline because it also updates the HUD Text texture
	float getMoveRotation(){return moveRotation;}										// inline function that returns the moveRotation this is NOT the models rotation but the rotation that the Move functions work off so the move functions know the current distance the models facing
	void updateMoveRotation(float rot){moveRotation = rot;}							    // inline function that updates the moveRotation suddenly and shouldn't be used to rotate the character mostly for changing the initial settings									
	void rotateCharacter(float rot);											     	// function used to rotate the character it increments both the rotation and character rotation as these two values must be kept seperate for the move functions to move the character in the right distance
	void moveCharacterForwardOrBackward(float dist);								    // function that moves the model forward or backwards in the world based off of themoveRotation and moves by the passed in distance float
	void moveCharacterRightOrLeft(float dist);										    // function that moves the  model left or right in the world based off of the moveRotation and moves by the passed in distance float
	void updateCharacterAnimation(int anim);											// function that is used to update the PlayerGameObjects md2model'scurrent animation
	int getCharacterCurrentAnimation(){return model->getCurrentAnimation();}		    // function that is used to return the PlayerGameObjects md2model's current animation
	void drawHUD(GLuint shaderID);                                                      // function that takes in a shader ID to draw the PlayerGameObject's PlayerHUD to screen													

private:
	// ints/vec3s to store the score/hp/position/rotation/scalar of the object
	vec3 position;
	vec3 rotation;
	vec3 scalar;
	int score;
	int health;

	// SDL colour for the HUD
	SDL_Color textColour;
	
	// float for the Characters movement rotation not the models rotation, this is used for the Move functions
	float moveRotation;
	
	// pointers to PlayerHUD and the MD2ModelHolder objects
	PlayerHUD * characterHUD;
	MD2ModelHolder * model;

};

#endif