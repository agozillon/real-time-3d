#include "TextToTexture.h"
#include <iostream>   // for console output
#include "rt3d.h"	  // for exit fatal error function

// constructor that takes in a TTF_Font pointer and assigns it to textFont
TextToTexture::TextToTexture(TTF_Font *font)
{	
	textFont = font;
}

// destructor for deleting instantiated objects and in this case closing 
// the textFont
TextToTexture::~TextToTexture()
{
	TTF_CloseFont(textFont);
}

// function that takes in a set of chars and 3 ints representing RGB and then creates a
// texture version of the text and returns it
GLuint TextToTexture::createTextTexture(const char * str, SDL_Color colour) 
{	
	if (textFont == NULL)
	{
		rt3d::exitFatalError("failed to open font");
	}

	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)
	{
		std::cout << "String surface not created." << std::endl;
	}

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	
	if (colours == 4)  // alpha
	{  
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGBA;
		}
	    else
		{
		    format = GL_BGRA;
		}
	} 
	else // no alpha
	{             
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGB;
		}
	    else
		{
		    format = GL_BGR;
		}
	}

	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}
