#include "ThirdPersonCamera.h"


// constructor that takes a vec3 as a lookAt position for the ThirdPersonCamera, a follow distance for the camera 
// and an original camera rotation and then assigns them
ThirdPersonCamera::ThirdPersonCamera(vec3 lookAt, float followDist, float rot)
{
	// calling the update functions to assign the eye/at/up variables as it's more concise this way for the
	// Eye
	updateEye(vec3(lookAt.x, lookAt.y, lookAt.z-followDist));
	updateAt(lookAt);
	updateUp(vec3(0.0, 1.0, 0.0));
	
	// assigning rotation and eyeDistanceBehind variables
	rotation = rot;
	followDistance = followDist;
}

// function that when called updates the camera to the current EyeFollowDistance away from 
// the lookAt vec3 and the rotation around the lookAt depends on the current rotation
void ThirdPersonCamera::updateCamera(vec3 lookAt)
{
	at = rt3d::moveForward(lookAt, rotation, 1.0f);
	eye = rt3d::moveForward(lookAt, rotation, followDistance);
}
