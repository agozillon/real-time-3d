#include "BasicQuad.h"

// constructor that passes in the height and width to create a BasicQuad 
BasicQuad::BasicQuad(float h, float w)
{
	// setting indexCount and vertexCount to draw a quad
	indexCount = 6;
	vertexCount = 4;
	height = h;
	width = w;
	
	// hard coded indices for the quad
	GLuint quadIndices[] = 
	{
		0,3,2, 2,1,0, // quad clockwise 
	}; 

	// hardcoded quad tex coords anti-clockwise from top left of quad
	GLfloat quadTexCoords[] = 
	{ 
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
	};
	
	// slightly breaking indentation here so that the quads vertices are nice and easy to read
    quadVertices[0] = -width; quadVertices[1] = -height;  quadVertices[2] = 1;  // vert 1
	quadVertices[3] = -width; quadVertices[4] = height;   quadVertices[5] = 1;  // vert 2
	quadVertices[6] = width;  quadVertices[7] = height;   quadVertices[8] = 1;  // vert 3
	quadVertices[9] = width;  quadVertices[10] = -height; quadVertices[11] = 1; // vert 4

	// creating the quads mesh using the rt3d function and assigning it to the meshObject
	meshObject = rt3d::createMesh(vertexCount, quadVertices, nullptr, quadVertices, quadTexCoords, indexCount, quadIndices);
}

// call to draw the BasicQuad 
void BasicQuad::draw()
{
	rt3d::drawIndexedMesh(meshObject, indexCount, GL_TRIANGLES);
}
