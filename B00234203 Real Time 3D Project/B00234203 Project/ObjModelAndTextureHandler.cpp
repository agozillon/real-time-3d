#include "ObjModelAndTexutreHandler.h"
#include <iostream> // included for console output

// loads in and stores a texture using a passed in texture file name and a boolean checking if 
// it should be set to clamp to edge
void ObjModelAndTextureHandler::loadAndStoreTexture(char *fileName, bool clampToEdge)
{
	// pushing the textureID onto the textureIDList
	textureIDList.push_back(rt3d::loadBitmap(fileName, clampToEdge));
}

// takes in a obj model file name and loads in an obj file and then stores the 
// variables required to draw the obj model within a vector
void ObjModelAndTextureHandler::loadAndStoreObjModel(char *fileName)
{
	// temporary vectors to help load in the obj model
	std::vector<GLfloat> tempVerts;
	std::vector<GLfloat> tempNorms;
	std::vector<GLfloat> tempTexCoords;
	std::vector<GLuint>  tempIndices;

	// loads in the obj model
	rt3d::loadObj(fileName, tempVerts, tempNorms, tempTexCoords, tempIndices);
	
	// pushing the index count and mesh ID onto there respective vector lists
	objMeshIndexCountList.push_back(tempIndices.size());
	objMeshObjectList.push_back(rt3d::createMesh(tempVerts.size()/3, tempVerts.data(), nullptr, tempNorms.data(), tempTexCoords.data(), tempIndices.size(), tempIndices.data()));
	
}

// takes in two GLuints as a reference to a meshID and meshIndexCount to get the ID/Index count and an int as the position of the
// mesh within the vector
void ObjModelAndTextureHandler::getObjModel(GLuint &meshID, GLuint &indexCount, int posInVector)
{
	// if the position in the vector array is less than the maximum size of the IndexCount and Mesh list then
	// get the specificed values and set the passed in references to those values else output that the position
	// passed in is out of bounds
	if(posInVector <= objMeshIndexCountList.size() && objMeshObjectList.size())
	{
		indexCount = objMeshIndexCountList.at(posInVector);
		meshID = objMeshObjectList.at(posInVector);
	}
	else
	{
		std::cout << "position in vector array passed in is out of bounds";
	}
}

// takes in a GLuints as a reference to a texture to get the texture ID and an int as the position of the
// texture within the vector
void ObjModelAndTextureHandler::getTextureID(GLuint &textureID, int posInVector)
{
	// checks if the position in the vector is larger than the vectors size if it 
	// isn't set the passed in GLuint to the textureID else output that it's out
	// of the bounds of the vector
	if(posInVector<= textureIDList.size())
	{
		textureID = textureIDList.at(posInVector);
	}
	else
	{
		std::cout << "position in vector array passed in is out of bounds";
	}
}