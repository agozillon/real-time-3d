#ifndef	SKYBOX_H
#define SKYBOX_H
#include "rt3d.h"						// including rt3d for rt3d functions  
#include "BasicQuad.h"					// including BasicQuad to create my sky box walls
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for matrix transformations
#include <glm/gtc/type_ptr.hpp>			// including glm/gtc/type_ptr.hpp for glm types
using namespace glm;					// using glm name space so I don't have to include variablename

// a sky box class based off of the tutorial that uses a quad and takes in 5 textureIDs and then positions
// the quad in a box and turns off the depth mask to draw it 
class SkyBox
{

public:
	// constructor takes in 5 textureIDs for the walls
	SkyBox(GLuint topTextureID, GLuint leftTextureID, GLuint rightTextureID, GLuint backTextureID, GLuint frontTextureID); 	
	~SkyBox();										// basic class destructor deletes objects instantiated  
	void draw(GLuint shaderID, mat4 mvMatrix);	    // takes in a shaderID and matrix to draw the sky box

private:
	// array of GLuints to hold the textureIDs, and a BasicQuad to use as the SkyBox walls and roof
	GLuint texture[5];
	BasicQuad * textureQuad;
};

#endif

