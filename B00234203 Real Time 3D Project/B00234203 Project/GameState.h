#ifndef GAMESTATE_H
#define GAMESTATE_H

// SAME AS GED EXCEPT I TOOK OUT EXIT AND SDL EVENT FUNCTIONS AND RENAMED ENTRY TO ENTER
//I LEFT THE COMMENTS IN, INCASE ANYONE REVIEWING THIS CLASS NEEDS A RECAP.

// GameState needs to refer to the Game object which needs to
// refer to GameState objects, which creates a circular dependency
// and causes problems with #includes 
// solution is to declare (but not define) the Game type here.
// so instead of 
// #include "Game.h"
// we have a 'forward declaration' of the class:
class Game;
// Game.h will still need to be included in the state implementation files


// Abstract base class for GameState: no implementation as it is an abstract class and all of the games states shall inherit 
// from this and will require the following functions and objects 
class GameState
{

public:
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
    virtual void draw() = 0;                                       // function that draws objects to screen using the Game Window variable
    virtual void init() = 0;                                       // function that is used to initilize all of the States variables and objects
    virtual void update() = 0;                                     // function that is used to update the state (anything that happens while the stat is occuring should go in here)
    virtual void enter() = 0;									   // function that is used to set up certain things on entry like a reset function
	virtual ~GameState() {}                                        // blank destructor

};

#endif

