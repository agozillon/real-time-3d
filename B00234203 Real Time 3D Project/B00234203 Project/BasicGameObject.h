#ifndef SCENERYGAMEOBJECT_H
#define SCENERYGAMEOBJECT_H
#include "AbstractGameObject.h" // including the AbstractGameObject so that this class can inherit and also gets access to its includes
#include "ObjModelHolder.h"     // including the ObjModelHolder class so that we can create an object of this type within this class

//  BasicGameObject is a class that inherits from AbstractGameObject and implements the ObjModelHolder/Bounding Box class
// to store an Obj model position it on screen, draw it and setup a bounding box for collision
class BasicGameObject: public AbstractGameObject
{

public:
	// constructor that takes in the initial position/rotation/scalar of the BasicGameObject and takes in a meshID/indexcount/texutreID for the objModelHolder also takes in position and dimensions for a boundingbox
	BasicGameObject(vec3 pos, vec3 rot, vec3 scale, GLuint meshID, GLuint indexCount, GLuint textureID, vec3 collisionBoxPos, vec3 collisionBoxDimensions);
	~BasicGameObject();																 // class destructor
	vec3 getPosition(){return position;}									         //	inline function that returns the position as a vec3
	void updatePosition(vec3 pos){position = pos;}									 // inline function that takes a vec3 and updates the position
	vec3 getRotation(){return rotation;}										     //	inline function that returns the rotation as a vec3			 
	void updateRotation(vec3 rot){rotation = rot;}									 //	inline function that takes a vec3 and updates the rotation
	vec3 getScalar(){return scalar;}											     //	inline function that returns the scalar as a vec3
	void updateScalar(vec3 scale){scalar = scale;}									 //	inline function that takes a vec3 and updates the scalar
	void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix);		 // function that takes a shader/material/matrix and uses them to draw the model

private:
	// setting up 3 vec3s to hold the models position/rotation/scale
	vec3 position;
	vec3 rotation;
	vec3 scalar;

	// setting up a pointer to an ObjModelHolder type
	ObjModelHolder * model;

};

#endif	
