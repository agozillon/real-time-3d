#include "BassAudioLoader.h"
#include <iostream>       // iostream to output various messages within the class

// constructor that takes in an audio file name on construction initilizes the output device and loads in the audio
BassAudioLoader::BassAudioLoader(char * fileName)
{
	/* Initialize default output device */
	if (!BASS_Init(-1, 44100, 0, 0, NULL))
	{
		std::cout << "Can't initialize device or device already Initialized" << std::endl;
	}

	// calling the loadSample function
	sample = loadSample(fileName);
}

// private function that accepts an audio file name loads it in and creates a bass sample version of it 
HSAMPLE BassAudioLoader::loadSample(char * fileName)
{
	// creating a HSAMPLE to load in the audio and then return it
	HSAMPLE sam;
	
	// loading in the audio and checking if it worked correctly if it didn't then call c++ exit function
	if (sam=BASS_SampleLoad(FALSE, fileName, 0, 0, 3, BASS_SAMPLE_OVER_POS))
	{
		std::cout << "sample " << fileName << " loaded!" << std::endl;
	}
	else
	{
		std::cout << "Can't load sample";
		exit (0);
	}

	// return the sample
	return sam;
}

// plays the audio file on a randomly selected audio channel
void BassAudioLoader::playAudio()
{
	// getting a channel for the audio
	HCHANNEL ch=BASS_SampleGetChannel(sample, FALSE);

	// setting the audios channel to a random set of attributes
	BASS_ChannelSetAttributes(ch, -1, 50, (rand()%201)-100);

	// if the channel can't play then output can't play sample
	if (!BASS_ChannelPlay(ch, FALSE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}
