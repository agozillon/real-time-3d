#include "PlayerHUD.h"
#include <stack>    // including for access to the stack container

// constructer that takes in the height/width for the BasicQuad, it also takes in size and colour of the text, the positions of each HUD element and 2 texture ID's for the HUD images
PlayerHUD::PlayerHUD(float hOfElements, float wOfElements, int sizeOfText, SDL_Color colour, vec3 firstHUDTextPos, vec3 secondHUDTextPos, vec3 firstHUDImagePos, vec3 secondHUDImagePos, GLuint firstHUDImageTextureID, GLuint secondHUDImageTextureID)
{
	// setting the text/image positions
	hudTextPosition[0] = firstHUDTextPos;
	hudTextPosition[1] = secondHUDTextPos;
	hudImagePosition[0] = firstHUDImagePos;
	hudImagePosition[1] = secondHUDImagePos;

	// instantiating the text loader and opening the font, and then creating and assigning the textures
	textLoader = new TextToTexture(TTF_OpenFont("MavenPro-Regular.ttf", sizeOfText));
	texture[0] = textLoader->createTextTexture("Blank", colour);
	texture[1] = textLoader->createTextTexture("HUD", colour);
	texture[2] = firstHUDImageTextureID;
	texture[3] = secondHUDImageTextureID;

	// instantiating the basic quad for this class using the passed in values
	textureQuad = new BasicQuad(hOfElements, wOfElements);
	
}

// function to update the HUD text takes in a two sets of chars and then creates new textures in place
// of the old
void PlayerHUD::updateHUDText(const char * firstHUDText, const char * secondHUDText, SDL_Color colour)
{
	texture[0] = textLoader->createTextTexture(firstHUDText, colour);
	texture[1] = textLoader->createTextTexture(secondHUDText, colour);
}

// function to draw the HUD to screen and takes a passed in shader to do it, couldn't get it working by
// resetting projection/modelview to the identity matrix unfortunately. uses a single Quad to position and draw them all
void PlayerHUD::draw(GLuint shaderID)
{
	// turning depth mask off
	glDepthMask(GL_FALSE);
	
	std::stack<mat4> modelStack; 

	// setting the stack to the identity matrix and then binding the texure, positioning, 
	// setting uniformMatrix and then drawing the second HUD Text 
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	modelStack.push(mat4(1.0));
	modelStack.top() = translate(modelStack.top(), hudTextPosition[0]);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	// setting the stack to the identity matrix and then binding the texure, positioning, 
	// setting uniformMatrix and then drawing the second HUD Text 
	modelStack.push(mat4(1.0));
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	modelStack.top() = translate(modelStack.top(), hudTextPosition[1]);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();

	// setting the stack to the identity matrix and then binding the texure, positioning, 
	// setting uniformMatrix and then drawing the first HUD Image 
	modelStack.push(mat4(1.0));
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	modelStack.top() = translate(modelStack.top(), hudImagePosition[0]);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();

	// setting the stack to the identity matrix and then binding the texure, positioning, 
	// setting uniformMatrix and then drawing the second HUD Image 
	modelStack.push(mat4(1.0));
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	modelStack.top() = translate(modelStack.top(), hudImagePosition[1]);
	rt3d::setUniformMatrix4fv(shaderID, "modelview", value_ptr(modelStack.top()));
	textureQuad->draw();
	modelStack.pop();
	
	// set depth mask back to true
	glDepthMask(GL_TRUE);
}

// destructor deleting all objects instantiated in this class
PlayerHUD::~PlayerHUD()
{
	delete textLoader;
	delete textureQuad;
}
