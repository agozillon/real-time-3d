#ifndef ABSTRACTMODELHOLDER_H
#define ABSTRACTMODELHOLDER_H
#include "rt3d.h"    // including rt3d library so that derived classes do not need to include it and for access to materialstruct
using namespace glm; // using glm name space so I don't have to include variablename

// abstract class for the ModelHolder type simply dictates that anything derived must 
// have a way to draw the model it holds
class AbstractModelHolder
{

public:
	virtual ~AbstractModelHolder(){}														// virtual inline destructor
	virtual void draw(GLuint shaderID, rt3d::materialStruct material, mat4 mvMatrix) = 0;   // virtual void function that takes in a shader, material structure and matrix to draw the model

};

#endif