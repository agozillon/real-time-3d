#ifndef FIRSTPERSONCAMERA_H
#define FIRSTPERSONCAMERA_H
#include "AbstractCamera.h" // including the AbstractCameraClass so the FirstPersonCamera class can inherit from it and has access to glm lookAt and vec3s		
#include <glm/gtc/matrix_transform.hpp> // including matrix_transform for lookAt

// basic First Person Camera class that inherits from AbstractCamera and can be used to move a camera around a scene
class FirstPersonCamera: public AbstractCamera
{

public:
	FirstPersonCamera();                                       // constructor sets up default values for the first person camera
	~FirstPersonCamera(){}									   // blank inline destructor
	vec3 getEye(){return eye;}								   // inline function that returns the cameras Eye position as a vec3
	vec3 getAt(){return at;}								   // inline function that returns the cameras At position as a vec3
	vec3 getUp(){return up;}								   // inline function that returns the cameras Up position as a vec3
	void updateEye(vec3 pos){eye = pos;}					   // inline function that updates the Eye position via a passed in vec3 
	void updateAt(vec3 pos){at = pos;}						   // inline function that updates the At position via a passed in vec3 
	void updateUp(vec3 pos){up = pos;}		                   // inline function that updates the Up position via a passed in vec3 
	void updateRotation(float rot);							   // function that updates the cameras rotation and renews the camera
	float getRotation(){return rotation;}					   // inline function that returns the cameras rotation as a float
	mat4 getCurrentCamera(){return lookAt(eye, at, up);}	   // inline function that returns the cameras LookAt as a mat4
	void moveForwardOrBackwards(float dist);				   // function used to move the camera forwards or backwards by a passed in distance
	void moveRightOrLeft(float dist);						   // function used to move the camera left or right by a passed in distance
	void moveUpOrDown(float dist);							   // function used to move the camera up or down by a passed in distance

private:
	
	// variables to hold the cameras Rotation/Eye/At/Up  
	float rotation;

	vec3 eye;
	vec3 at;
	vec3 up;

};

#endif