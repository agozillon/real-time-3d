#ifndef	PLAYERHUD_H
#define PLAYERHUD_H
#include "rt3d.h"							// including rt3d for rt3d functions  
#include "BasicQuad.h"						// including BasicQuads so I can make the HUD elements by putting the textures on them
#include "TextToTexture.h"					// including TextToTexture to create and update the text textures, also gives access to SDL_colour
#include <glm/gtc/matrix_transform.hpp>     // including and glm matrix_transform
#include <glm/gtc/type_ptr.hpp>				// including glm/gtc/type_ptr for vec3s
using namespace glm;						// using glm name space so I don't have to include variablename

// class to setup the player hud it takes in 2 textures and creates 2 sets of text textures
// applies them to a BasicQuad and positions them on the screen, Ideally the HUD would be made up of multiple 
// hud elements instead of having all the elements created within the class, however I didn't have time to make them
// seperate classes/objects
class PlayerHUD
{

public:
	// PlayerHUD constructor takes in a height/width for each quad the size and colour of the text being created and vec3 positions to position the various HUD elements, it also takes in 2 texture ID's for 2 hud images
	PlayerHUD(float hOfElements, float wOfElements, int sizeOfText, SDL_Color colour, vec3 firstHUDTextPos, vec3 secondHUDTextPos, vec3 firstHUDImagePos, vec3 secondHUDImagePos, GLuint firstHUDImageTextureID, GLuint secondHUDImageTextureID);
	~PlayerHUD();																    			  // destructor that deletes instantiated objects
	void draw(GLuint shaderID);																      // draw function that takes in a shader ID and draws all the elements based off of positions passed in during construction																																		
	void updateHUDText(const char * firstHUDText, const char * secondHUDText, SDL_Color colour);  // updates takes in two const char points and an SDL_Colour and updates the current text texture with the passed in characters and colour
	vec3 getFirstHUDTextPosition(){return hudTextPosition[0];}									  // inline function returns the first HUD Text Position as a vec3
	vec3 getSecondHUDTextPosition(){return hudTextPosition[1];}									  // inline function that returns the second HUD Text Position as a vec3
	vec3 getFirstHUDImagePosition(){return hudImagePosition[0];}								  // inline function that returns the first HUD Images Position as a vec3
	vec3 getSecondHUDImagePosition(){return hudImagePosition[1];}								  // inline function that returns the second HUD Images Position as a vec3
	void updateFirstHUDTextPosition(vec3 pos){hudTextPosition[0] = pos;}						  // inline function that updates the first HUD text Position with a passed in vec3
	void updateSecondHUDTextPosition(vec3 pos){hudTextPosition[1] = pos;}						  // inline function that updates the second HUD text Position with a passed in vec3
	void updateFirstHUDImagePosition(vec3 pos){hudImagePosition[0] = pos;}					      // inline function that updates the first HUD images Position with a passed in vec3
	void updateSecondHUDImagePosition(vec3 pos){hudImagePosition[1] = pos;}					      // inline function that updates the second HUD images Position with a passed in vec3

private:
	// GLuint that holds the 4 texture ID's required for the hud
	GLuint texture[4];

	// vec3s to hold all the HUD elements positions
	vec3 hudTextPosition[2];
	vec3 hudImagePosition[2];

	// pointers to a TextToTexture object to load in and update the text textures and a BasicQuad pointer to create a quad to map the textures to and position 
	TextToTexture * textLoader;
	BasicQuad * textureQuad;
};

#endif

